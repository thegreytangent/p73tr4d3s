<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('register', 'Api\RegisterController@register');
Route::post('AccountSetupRegistration', 'Api\RegisterController@AccountSetupRegistration');
Route::get('login', 'Api\RegisterController@login');
Route::post('watchlistcreate', 'Api\WatchlistController@watchlistcreate');
Route::get('GetBuyTotal', 'Api\TradePeformanceController@GetBuyTotal');
Route::get('GetWatchList', 'Api\WatchlistController@GetWatchList');
Route::get('watchlist_get', 'Api\WatchlistController@watchlist_get');
Route::put('watchlist_update', 'Api\WatchlistController@watchlist_update');
Route::any('buttonlikes', 'Api\WatchlistController@likes');
Route::any('comment', 'Api\WatchlistController@comment');
Route::any('Ifollow', 'Api\followsController@Ifollowed');
Route::any('followers', 'Api\followsController@follower');
Route::post('InsertBuy', 'Api\TradePeformanceController@InsertBuy');
Route::post('InsertSell', 'Api\TradePeformanceController@InsertSell');
Route::get('getEntryDetail', 'Api\TradePeformanceController@getEntryDetail');
Route::any('EditDelete', 'Api\TradePeformanceController@EditDelete');
Route::any('buttonFollow', 'Api\followsController@buttonFollow');



Route::middleware('auth:api')->group( function () {
Route::resource('products', 'Api\ProductController');
Route::get('UserProfile', 'Api\UserInfornationController@UserProfile');
  Route::post('UserInfoUpdate', 'Api\UserInfornationController@UserInfoUpdate');
  Route::get('Userinfo', 'Api\UserInfornationController@Userinfo');


  // Route::get('GetUser/{user_id}', 'Api\UsersRulesController@getUserData');
  Route::get('ActiveTraders', 'Api\MostActiveTradersController@MostActive');
  Route::get('YouMayKnow', 'Api\MostActiveTradersController@YouMayKnow');

  // Route::get('GetTicker/', 'Api\TradePeformanceController@GetTicker');
  Route::get('BuyAPI/{buy}', 'Api\TradePeformanceController@GetBuyRecord');

  Route::get('GetDetailBuy/{data}', 'Api\TradePeformanceController@GetDetailBuy');



  //=============================== My Performance ====================================================
  Route::get('UpdateBuy/{data}', 'Api\TradePeformanceController@UpdateBuy');
  Route::get('DeleteData/{data}', 'Api\TradePeformanceController@DeleteData');
  Route::get('chartPerDay/{data}', 'Api\TradePeformanceController@chartPerDay');
  Route::get('SellTable/{data}', 'Api\TradePeformanceController@SellTable');
  Route::get('sell', 'Api\TradePeformanceController@getSellRecords');
  //=============================== end of  Performance =============================================

  //=============================== My Trade Details Routes ======================================
  Route::get('trade-details/{UserId}/{view}','Api\TraideDetailsController@getTrades');
  Route::get('trade-details/update/{sellId}/{panelNumber}','Api\TraideDetailsController@show');
  Route::put('trade-details','Api\TraideDetailsController@update_details');
  //=============================== End of My Trade Details Routes ================================


  //=============================== My Strategies/Rules Routes ======================================
  Route::get('strategy-rules/{UserId}','Api\StrategiesRulesController@getStrategyRules');
  Route::post('strategy-rules','Api\StrategiesRulesController@createStrategiesRules');
  Route::put('strategy-rules','Api\StrategiesRulesController@updateStrategiesRules');
  //=============================== End of My Strategies/Rules Routes ================================


  //=============================== My User Settings ======================================
  Route::get('UserSettings/privacy/{user_id}','Api\UserSettingsController@getPrivacySettings');
  Route::put('UserSettings/update_personal_info','Api\UserSettingsController@updatePersonalInfo');
  Route::put('UserSettings/update_linksTradesInfo','Api\UserSettingsController@updateLinksTradesInfo');
  Route::put('UserSettings/update_privacy','Api\UserSettingsController@updatePrivacySettings');
  //=============================== End of My User Settings ================================


  //===============================  User ======================================
    Route::get('user','Api\UserController@GetUserInfobyUsername');
  //=============================== end of User ======================================


//===============================  Bookamarks =====================================
  Route::get('bookmarks/{user_id}','Api\BookmarksController@getBookmarks');
//===============================  end of Bookamarks ==============================


  Route::get('CreateRoom/{data}', 'Api\RoomController@CreateRoom');
  Route::get('ApproveRequest/{data}', 'Api\RoomController@ApproveRequest');


  //binance
  Route::get('binance', 'Api\TraideDetailsController@binance');

  Route::get('GetTicker', 'Api\TickerController@GetTicker');

  //getStrategyRules
  Route::get('getStrategyRules', 'Api\StrategyRulesController@getStrategyRules');



  Route::get('follow/{user_id}', 'Api\FollowsController@getFollowing');


  //Global Controller
  Route::post('likes','Api\LikesController@getLike');
  Route::post('comment','Api\CommentsController@getComment');
  Route::get('search_user','Api\SearchController@SearchUser');

});

//=============================== No auth ======================================
Route::get('DashboardWatchlist/{user_id}','Api\BookmarksController@getDashboardWatchlist');
Route::resource('brokers', 'Api\BrokersController');
Route::get('UserSettings/{user_id}','Api\UserSettingsController@getSettings');
Route::get('checkEmail','Api\SearchController@checkEmail');
Route::resource('tradingStyle', 'Api\TradingStyleController');
//===============================  ===============================
