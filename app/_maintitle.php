<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class _maintitle extends Model
{
    protected $table = '_maintitle';

    protected $fillable = [
        'mainTitle','icon'
    ];
}
