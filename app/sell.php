<?php

namespace App;
use App\trade_details;
use App\buy as Buy;

use App\trade_details_images;
use App\trade_details_comments;
use App\tickers;
use App\UsersModel as User;
use App\Db_Objects;



class sell extends Db_Objects
{
	protected $table = 'sell';


	protected $fillable = [
		'Id','UserId','TickerId','Volume','BuyDate','BuyAmount','SellDate','SellAmount','TransactionId'
	];

	protected static
	$brokerCommission = 0.0025,
	$vat = 0.12,
	$securityClearing = 0.0001,
	$salesTax = 0.006,
	$pseFee = 0.00005;




	public function displayBuyAmount()
	{

		$result = ($this->BuyAmount <= 1 ) ? number_format($this->BuyAmount, 4) : number_format($this->BuyAmount, 2);
		return $result;
	}

	public function show_InitialEntryDate()
	{
		return date('M. d,Y', strtotime($this->getInitialEntryDate()));
	}

	public function entryDate()
{
	$link = '------';
	return (!$this->BuyDate || $this->BuyDate < 1) ? $link : date('m/d/y', strtotime($this->BuyDate));
}





	public function getExitDate()
	{
		// $timeStamp = strtotime($this->SellDate);
		// return Date('M d, Y', $timeStamp);
		return $this->SellDate;
	}

	public function getExitDateTimeStamps() {
		return strtotime($this->SellDate)*1000;
	}

	public function getInitialEntryDate()
	{
		$array = array();
		$getdate = Buy::findbyUserIdAllShareUniqueId($this->UserId, $this->TransactionId);
		// array_reverse($getdate);
		foreach ($getdate as $date ) {
			$array[] = $date->getDateTime();
		}
		// sort($array);

		return $array[0];
	}



	public function getDetailsContent() {
		$detail = trade_details::where('SellId','=', $this->Id)->first();
		return (!$detail) ? "" : $detail->Content;
	}

	public function getDetailsId() {
		$detail = trade_details::where('SellId','=', $this->Id)->first();
		return (!$detail) ? 0 : $detail->Id;
	}


	public function getTickerCode() {
		$ticker = tickers::where('Id','=',$this->TickerId)->first();
		return ($ticker) ? $ticker->Code : 'Ticker not found';
	}

	public function getDetailsImages() {
		$detail_images = trade_details_images::where('SellId','=', $this->Id)->get(['Id','Image']);
		return (!$detail_images) ? [] : $detail_images;
	}


	public function getDetailsLikes($userId) {
		$likes = [];
		$countLikes  = trade_details_likes::countLikes($this->Id);
		$detail_likes = trade_details_likes::where([['UserId','=',$userId],['SellId','=',$this->Id],])->first();
		$didILike = ($detail_likes) ? true : false;
		$id = ($detail_likes) ? $detail_likes->Id : null;
		$likes = array(
			'countLikes' => $countLikes,
			'didILike' => $didILike,
			'id' => $id
		);
		return $likes;
	}





	public function getDetailsComments() {

		$detailsComments = new trade_details_comments();
		$detailsComments->UserId = $this->UserId;
		$detailsComments->SellId = $this->Id;

		$comments = array();

		if ($detailsComments->hasComments()) {
			foreach ( $detailsComments->findBySell()->get() as $comment) {
				$user = User::find_by_id($comment->UserId);
				$comments[] = array(
					'sellId' => $comment->SellId,
					'userId' => $user->id,
					'user'	 => $user->completeName(),
					'comment' => $comment->Comment,
					'date'	 => getDateOnly($comment->updated_at)
				);
			}
		}

		return $comments;
	}


	public function percentageGain(){
		return (float)($this->computeNet() - ($this->BuyAmount * $this->Volume)) / ($this->BuyAmount * $this->Volume) * 100;
	}


	public function percentGain()
{
  return number_format($this->percentageGain(), 2);
}




	public function computeDeductions(){
		$deductions = array(
			$this->computeBrokerComission(),
			$this->computeVat(),
			$this->computeSecurityClearing(),
			$this->computeSalesTax(),
			$this->computePseFee()
		);
		return array_sum($deductions);
	}

	public static function countWins($userId) {
		$trades = self::where('UserId','=', $userId)->get();
		$count = 0;
		foreach ($trades as $trade) {
			if ($trade->computeGain() > 0 ) {
				$count++;
			}
		}
		return ($count <= 0 ) ? 0 : $count;
	}

	public static function countLosses($userId) {
		$trades = self::where('UserId','=', $userId)->get();
		$count = 0;
		foreach ($trades as $trade) {
			if ($trade->computeGain() < 0 ) {
				$count++;
			}
		}
		return ($count <= 0 ) ? 0 : $count;
	}

	public static function winning_percentage($userId) {
		return  (self::countWins($userId) / self::total_trades($userId));
	}


	public static function total_trades($userId) {
		return self::countWins($userId) + self::countLosses($userId);
	}


	public static function find_by_losses($userId) {
		$trades = self::where('UserId','=', $userId)
		->where('BuyAmount','<', 'SellAmount')->get();
		return $trades;
	}




	public function computeGain(){
		return $this->computeNet() - ($this->BuyAmount * $this->Volume);
	}

	public function getGain()
{
 return number_format($this->computeGain(), 2);

}



  public function display_holdingPeriod() {
    $holding =  $this->calculateHoldingPeriod();
    if ($holding == 0 ) {
      $display = 'Intra';
    }
    elseif ($holding == 1 ) {
      $display = $holding." day";
    }
    elseif ($holding < 0 ) {
      $display = "n/a";
    }
    else {
      $display = $holding . " days";
    }
    return $display;
  }


  public function calculateHoldingPeriod() {
    $startDate = strtotime($this->BuyDate);
    $endDate = strtotime($this->SellDate);
    if (empty($startDate) || ($startDate == "")) {
      $result = -1;
    } else {
      $datediff = $endDate - $startDate;
      $result =  floor($datediff / (60 * 60 * 24));
    }
    return $result;
  }





	public function computeGross(){
		return $this->Volume * $this->SellAmount;
	}

	public function computeNet() {
		return $this->computeGross() - $this->computeDeductions();
	}


	public function gain(){
		return number_format($this->computeGain(), 2);
	}

	public function getPercentGain() {
		return number_format($this->percentageGain(), 2);
	}

	private function computeBrokerComission(){
		$compute = $this->computeGross() * self::$brokerCommission;
		return ($compute < 20) ? 20 : $compute;
	}

	private function computeVat(){
		return $this->computeBrokerComission() * self::$vat;
	}

	private function computeSecurityClearing(){
		return $this->computeGross() * self::$securityClearing;
	}

	private function computeSalesTax(){
		return $this->computeGross() * self::$salesTax;
	}

	private function computePseFee(){
		return $this->computeGross() * self::$pseFee;
	}


	public static function findbyUserOrderByDateDesc($userId)
	{
		$result = self::where('UserId','=', $userId)->orderBy('SellDate', 'DESC')->get();
		return (!$result) ? FALSE : $result;
	}


	public function getId() {
		return (int)$this->Id;
	}

	public function getTransactionId() {
		return $this->TransactionId;
	}

	public function getVolume()
{
  return $this->Volume;
}


	public function getExit()
{
  $result = ($this->SellAmount < 1 ) ? number_format($this->SellAmount, 4) : number_format($this->SellAmount, 2);
  return $result;
}

public static function findbyUserOrderByDateAsc($userId)
  {

		$result = self::where('UserId','=', $userId)->orderBy('SellDate', 'ASC')->get();
		return (!$result) ? FALSE : $result;
  }
























}
