<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class room_users extends Model
{
    protected $table = 'room_users';
    protected $fillable = [
        'room_name','user_id','user_name','user_image','key','status'
    ];
}
