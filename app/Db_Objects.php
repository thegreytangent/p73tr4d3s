<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Db_Objects extends Model {



  // ===================== For Global Query ==========================
  public static function findByColumnArray($column,$id) {
		$result = self::where($column,'=', $id);
		return ($result) ? $result->get() : FALSE;
	}

  public static function findByColumn($column,$id) {
    $result = self::where($column,'=', $id);
    return ($result) ? $result->first() : FALSE;
  }

  public static function find_by_id($id) {
    $result = self::where('id','=', $id);
		return ($result) ? $result->first() : FALSE;
  }

  public static function update_data($column,$id,$updateData = []) {
      $result = self::where($column,$id)->update($updateData);
      return ($result) ? TRUE : FALSE;
  }

  public static function delete_data($column,$id) {
    return (self::where($column, $id)->delete()) ? TRUE : FALSE;
  }


  public static function find_by_user($id) {
  $result = self::findByColumn('UserId',$id);
  return (!$result) ? FALSE : $result;
}






// ===================== Reuseable Methods =========================
// ===================== Strategy and Rules =========================
public static function findByUserAndStratId($userId,$stratId) {
  $result = self::where([['UserId','=',$userId],['StrategyRulesId','=',$stratId],]);
  return (!$result) ? FALSE : $result->first();
}




















}
