<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class _user_maintitle extends Model
{
    protected $table = '_user_maintitle';

    protected $fillable = [
        'user_mainTitle_id','mainTitle','icon'
    ];
}
