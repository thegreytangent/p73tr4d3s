<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class watchlist_likes extends Model
{
    protected $table = 'watchlist_likes';

    protected $fillable = [
        'WatchlistId','UserId'
    ];
}
