<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trade_details extends Model
{
    protected $table = 'trade_details';

    protected $fillable = ['Id','SellId','Content','Date'];

  
    public static function updateBySell($sellId,$data) {
    	return trade_details::where('SellId', $sellId)->update($data);
    	// return $data;
    }

    public static function find_details($sellId) {
    	return self::where('SellId','=', $sellId)->first();
    }
   


}
