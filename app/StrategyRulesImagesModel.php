<?php
namespace App;
use App\User;
use App\Db_Objects;

class StrategyRulesImagesModel extends Db_Objects {
  protected $table = 'strategy_rules_images';

  public function create() {
    return self::insert(array(
      'StrategyRulesId' => $this->StrategyRulesId,
      'Images'          => $this->Images,
      'created_at'      => getDateTimeNow()
    )) ? TRUE : FALSE;
  }


  	public function isNew() {
  		return self::find($this->Id) ? false : true;
  	}



  public static function deleteByStrategy($id) {
    	return self::where('StrategyRulesId',$id)->delete() ? TRUE : FALSE;
  }


}
