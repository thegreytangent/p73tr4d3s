<?php


namespace App\Http\Controllers\API;
use Auth;
use App\User;
use App\_modules;
use App\_user_maintitle;
use App\users_information;
use App\sell;
use App\watchlist;
use App\tickers;
use App\buy;
use App\watchlist_comments;
use App\watchlist_likes;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use Illuminate\Support\Facades\DB;


class WatchlistController extends BaseController
{
    public function GetWatchList(){
     $watchList = watchlist::select('UserId','Content','Id','Image','TickerId')->where('UserId',request('user_id'))->get();
     $watchListData = array();
     $images = array();
      foreach($watchList as $row) {
        //comment
        $watchlist_comments =  watchlist_comments::select('Comment','UserId','Date','Id')->where('WatchlistId','=',$row['Id'])->get();
        $comment = array();
        $likes = '';
        $GetUser1= User::select('users.id','users_information.Image','users.Firstname', 'users.Lastname')
        ->join('users_information','users_information.UserId','=','users.id')
        ->where('users.id',$row['UserId'])->first();
       
        foreach($watchlist_comments as $row2) {

            $GetUser2= User::select('users.id','users_information.Image','users.Firstname', 'users.Lastname')
                        ->join('users_information','users_information.UserId','=','users.id')
                        ->where('users.id',$row2['UserId'])->first();
            

          $data['comment'] = $row2['Comment'];
          $data['date'] =  date("d-M-Y", strtotime( $row2['Date']));
          $data['user'] = $GetUser2->Firstname . ' ' . $GetUser2->Lastname;
          $data['img'] = '../assets/img/'.$GetUser2->Image;

          $comment[] = $data;
        }
           $images = watchlist::select(DB::raw('CONCAT("../assets/img/","",Image) as Image'))->where('Id',$row['Id'])->first();
           if (empty($images->image)) {
            $images_q = watchlist::select('Id',DB::raw('CONCAT("","",img_watch) as Image'))->where('Id',$row['Id'])->get();
           }else{
            $images_q = watchlist::select('Id',DB::raw('CONCAT("../assets/img/","",Image) as Image'))->where('Id',$row['Id'])->get();
           }
         

          $watchlist_likes=  watchlist_likes::select( DB::raw('COUNT(*) as Likes'))->where('WatchlistId','=',$row['Id'])->get(); 
          foreach($watchlist_likes as $row3) {
            if ($row3['Likes'] > 0) {
                $didILike = true;
            }else{
              $didILike = false;
            }
            $data1['countLikes'] = $row3['Likes'];
            $data1['didILike'] = $didILike;
            $likes = $data1;
          }
          $tickerDesc = tickers::select(DB::raw("Code"))->where('Id','=',$row['TickerId'])->first();  

          //array response
          $list['comments'] = $comment;
          $list['content'] = $row['Content'];
          $list['id'] = $row['Id'];
          $list['images']=$images_q;
         
          $list['user_name'] = $GetUser1->Firstname . ' ' . $GetUser1->Lastname;;
          $list['user_id'] = $GetUser1->id;
          $list['likes'] = $likes;
          $list['ticker_id'] = $row['TickerId'];
          $list['ticker_name'] = $tickerDesc->Code;
         $watchListData[] = $list;
      }
     
      if (empty($watchListData)) {
        // $response = array('response'=>'No Watchlist');
        $response = array();
        return $response;
      }else{
        return $watchListData;
      }

    }

    //insertion

 public function watchlistcreate(Request $request){
  
  foreach($request->images as $row) {
    $data[] = array('UserId'=>$request->user_id,
    'TickerId'=>$request->ticker_id,
    'Content'=>$request->content,
    'img_watch'=>$row['image']);
  }
        

               try {
                  $watchList = watchlist::insert($data);
                  return array('alert'=>'Watchlist succesfully added!','cmd'=>'true');
              } catch (\Illuminate\Database\QueryException $e) {
                  return array('alert'=>'Image too large!!','cmd'=>'false');
              }

              // return $data;
        
 }


  public function watchlist_get(){
    $watchList = watchlist::select('UserId','Content','Id','Image','TickerId')->where('Id',request('watchlistId'))->get();
    $AllData = array();
    foreach($watchList as $row) {

      $images = watchlist::select(DB::raw('CONCAT("../assets/img/","",Image) as image'))->where('Id',$row['Id'])->first();
      if (empty($images->image)) {
        $images_q = watchlist::select('Id',DB::raw('CONCAT("","",img_watch) as image'))->where('Id',$row['Id'])->get();
      }else{
        $images_q = watchlist::select('Id',DB::raw('CONCAT("../assets/img/","",Image) as image'))->where('Id',$row['Id'])->get();
      }


      $list['id'] = $row['Id'];
      $list['user_id'] = $row['UserId'];
      $list['ticker_id'] = $row['TickerId'];
      $list['content'] = $row['Content'];
      $list['images'] = $images_q;
    }

    return $list;
  }

  public function watchlist_update(Request $request){
    DB::table('watchlist')->where('Id', '=', $request->id)->delete();
    foreach($request->images as $row) {
        try {
        
          watchlist::where('Id', $request->id)
          ->update([ 'TickerId'=>$request->ticker_id,'Content'=>$request->content,'img_watch'=>$row['image']]);
          return array('alert'=>'Watchlist succesfully added!','cmd'=>'true');
      } catch (\Illuminate\Database\QueryException $e) {
          return array('alert'=>'Error Update!!!','cmd'=>'false');
      }
     

    }
  
  }


 public function likes(Request $request){

 
      //if exist
    $likers =  watchlist_likes::where('WatchlistId',$request->data_id)->where('UserId',$request->userid)->count();
    if ($likers==0){
      $data[] = array('WatchlistId'=>$request->data_id,'UserId'=>$request->userid);
      watchlist_likes::insert($data);
      return 'true';
      // return '0';
    }else{
      $list =  DB::table('watchlist_likes')->where('WatchlistId', '=', $request->data_id)->delete();
      return 'false';
      // return '1';
    } 

    // return $likers;
     
  }


  public function comment(Request $request){
      //watchlist_comments
      if($request->data=='watchlist'){
        $data[] = array('Comment'=>$request->comment,
        'WatchlistId'=>$request->data_id,
        'UserId'=>$request->user_id);
  
        watchlist_comments::insert($data);
      }

      return array('alert'=>'comment','cmd'=>'true');
    // return $request->all();//
     


  }

   
}
