<?php



namespace App\Http\Controllers\API;



use Illuminate\Http\Request;



use App\Http\Controllers\API\BaseController as BaseController;

use App\UsersModel as User;

use App\users_information as UserInfo; 

use App\users_links as Links;

use App\users_broker as UserBrokers;

use App\UsersTradingStyleModel as TradingStyle;

use App\PrivacySettingsModel as Privacy;





class UserSettingsController extends BaseController

{



	public function getSettings(int $userId) {		$settings = array();
		$user = User::find_by_id($userId);
		if ($user) {
			$settings =  array(
				"myId" => $user->id,
				"profile" => array(
					"links" => array(
						"facebook" 	=> $user->getfacebook(),
						"linkedin" 	=> $user->getLinkedin(),
						"twitter" 	=> $user->getTwitter(),
						"link1" 	=> $user->getLink1(),
						"link2" 	=> $user->getLink2(),
						"link3" 	=> $user->getLink3(),
					),
					"personal" 	=> array(
						"aboutMe" 			=>  $user->getAboutMe(),
						"address" 			=>  $user->getAddress(),
						"email" 			=>  $user->getEmail(),
						"firstname" 		=>  $user->getFirstname(),
						"lastname" 			=>  $user->getLastname(),
						"name" 				=>  $user->completeName(),
						"numbersFollowers" 	=>  $user->countFollowers(),
						"numbersFollowing" 	=>  $user->countFollowing(),
						"user_image" 		=>  $user->getImage()
					),
					"tradesInformation" => array(
						"brokers" => $user->getBrokers(),
						"styleOfTrading" => $user->getTradingStyleId()
					)
				)
			);
		}
		return $settings;

	}





	public function getPrivacySettings(int $userId) {

		$privacy = Privacy::find_by_user($userId);

		$privacy = array(

			"emailAddress" =>  $privacy->getEmail(),

			"accountPrivacy" =>  $privacy->getAccount(),

			"address" =>  $privacy->getAddress(),

			"user_id" =>  $privacy->getUserId(),

			"tradeDetails" =>  $privacy->getTradeDetails(),

			"tradePerformance" => $privacy->getTradePerformance(),

			"tradeStats" =>  $privacy->getTradeStats()

		);

		return $privacy;

	}





	public function updatePrivacySettings(Request $get) {

		$user_id = $get->user_id;



		$privacy = new Privacy;

		$privacy->setUserId($user_id);

		$privacy->setEmail($get->emailAddress);

		$privacy->setAccount($get->accountPrivacy);

		$privacy->setAddress($get->address);

		$privacy->setTradeDetails($get->tradeDetails);

		$privacy->setTradePerformance($get->tradePerformance);

		$privacy->setTradeStats($get->tradeStats);



		if (User::hasPrivacyData($user_id)) {

			$response = ($privacy->update_privacy()) ? success() : fail('Update failed');

		}else {

			$response = ($privacy->create_privacy()) ? success() : fail('Update failed');

		}



		return $response;

	}





	public function updatePersonalInfo(Request $get) {

		$user = User::find_by_id($get->UserId);

		$user->setFirstname($get->firstname);

		$user->setLastname($get->lastname);

		$user->setAboutMe($get->aboutme); 

		$user->setEmail($get->email);

		$userInfo = Userinfo::findByColumn('UserId',$get->UserId);

		$userInfo->setImage($get->image);

		$userInfo->setAddress($get->address);

		$userInfo->setAboutMe($get->aboutme);

		$result = $user->update_users() && $userInfo->update_info();

		return ($result) ? success() : fail();

	}







	public function updateLinksTradesInfo(Request $get) {

		$userLink = Links::find_by_user($get->myId);

		$userLink->setFacebook($get->facebook);

		$userLink->setTwitter($get->twitter);

		$userLink->setLinkedIn($get->linkedin);



		$UserBroker = new UserBrokers();

		$UserBroker->setUserId($get->myId);

		if ($UserBroker->hasBroker()) {

			$UserBroker->deleteExistingBroker();

		}

		$error = [];

		foreach ($get->brokers as $newBroker) {

			$UserBroker->setBrokerId($newBroker['id']);

			$error[] = (!$UserBroker->createNewBroker()) ? 1 : 0;

		}



		$userBrokerHasErrors = (array_sum($error) > 0 ) ? true : false;



		$tradeStyle = new TradingStyle();

		$tradeStyle->setUserId($get->myId);

		$tradeStyle->setStyleId($get->styleOfTrading);





		$result = $userLink->update_links() && !$userBrokerHasErrors && $tradeStyle->saveStyle();

		return ($result) ? success() : fail();

		

	}















	



	



}

