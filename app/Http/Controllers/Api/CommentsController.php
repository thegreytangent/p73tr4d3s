<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;

use App\trade_details_comments;
use App\StrategyRulesCommentsModel as StratComment;

class CommentsController extends BaseController
{
	public function getComment(Request $request) {
		$userId = $request->user_id;
		$comment = $request->comment;
		$refId = $request->data_id;
		$table = $request->data;

		switch ($table) {
			case 'trade-details':
			return $this->TradeDetailsCommentsCreate($userId,$comment,$refId);
			break;
			case 'strategy-rules':
			return $this->StrategyRulesComment($userId,$comment,$refId);
			break;
		}
	}


	public function StrategyRulesComment($userId,$comment,$stratId) {
		$create = StratComment::insert([
			'StrategyRulesId' => $stratId,
			'UserId' => $userId,
			'Comment' => $comment,
			'created_at' => getDateTimeNow(),
			'updated_at' => getDateTimeNow()
		]);

		if (!$create) {
			$response =  $this->sendResponse('failed', 'failed Comment');
		} else {
			$response =  $this->sendResponse([], 'success');
		}
		return $response;
	}


	public function TradeDetailsCommentsCreate($userId,$comment,$sell_id) {
		$create = trade_details_comments::insert([
			'SellId' => $sell_id,
			'UserId' => $userId,
			'Comment' => $comment,
			'created_at' => getDateTimeNow(),
			'updated_at' => getDateTimeNow()
		]);

		if (!$create) {
			$response =  $this->sendResponse('failed', 'failed Comment');
		} else {
			$response =  $this->sendResponse([], 'success');
		}
		return $response;

	}



}
