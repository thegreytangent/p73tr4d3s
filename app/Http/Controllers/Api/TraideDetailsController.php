<?php
namespace App\Http\Controllers\API;
use Auth;
use App\UsersModel as User;
use App\_modules;
use App\_user_maintitle;
use App\users_information;
use App\trade_details_images;
use App\sell;
use App\watchlist;
use App\trade_details;
use App\tickers;
use App\buy;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use Illuminate\Support\Facades\DB;
class TraideDetailsController extends BaseController
{
	protected static
	$brokerCommission = 0.0025,
	$vat = 0.12,
	$securityClearing = 0.0001,
	$salesTax = 0.006,
	$pseFee = 0.00005;
	private $ticker;
	public static $table = 'trade_details';



	public  function __construct() {
		$this->ticker = new tickers;
	}


	public function getTrades($UserId,$view) {
		//new code
		switch ($view) {
			case 'all':
			return $this->showAllTrades($UserId,$view);
			break;
			case 'allWins':
			return $this->showAllWinningTrades($UserId,$view);
			break;
		}

		return $result;

	}



	public function showAllTrades($getUser,$view) {
		$user = User::find_by_UsernameOrId($getUser);
		$UserId = $user->getId();
		$filter = 'all';
		$userSell = Sell::where('UserId','=', $UserId)->orderBy('id', 'desc')->paginate(6);
		$trades = array();
		$panel_number = self::countFilterOptions($UserId,$filter) - self::start_page();

		foreach ($userSell as $sell ) {
			$trades[] = $this->getSellCollections($sell,$panel_number);
			$loss = 0;
			$panel_number--;
		}

		$api = array(
			'trades' 		=> $trades,
			'total_trades' 	=> self::countFilterOptions($UserId,$filter),
			'wins' 			=> Sell::countWins($UserId),
			'losses' 		=> Sell::countLosses($UserId),
			'percent' 		=> Sell::winning_percentage($UserId)
		);
		return $api;
	}



	public function showAllWinningTrades($UserId,$view) {
		$filter = 'allWins';
		$userSell = Sell::where('UserId','=', $UserId)->orderBy('id', 'desc')->paginate(6);
		$trades = array();
		$panel_number = self::countFilterOptions($UserId,$filter) - self::start_page();

		foreach ($userSell as $sell ) {
			if ($sell->gain() > 0) {
				$trades[] = $this->getSellCollections($sell,$panel_number);
				$loss = 0;
				$panel_number--;
			}

		}

		$api = array(
			'trades' 		=> $trades,
			'total_trades' 	=> self::countFilterOptions($UserId,$filter),
			'wins' 			=> Sell::countWins($UserId),
			'losses' 		=> Sell::countLosses($UserId),
			'percent' 		=> Sell::winning_percentage($UserId)
		);

		return $api;
	}




	public static function start_page() {
		$startPage  = ($_GET['page'] !== 0) ? $_GET['page'] : 1;
		return  --$startPage * 6;
	}






	public static function countFilterOptions($UserId,$view) {
		$count = 0;
		$sell_array = array();
		foreach (Sell::where('UserId','=', $UserId)->get() as $sell) {
			if ($sell->gain() > 0 && $view=="allWins") {
				$count++;
			} elseif ($sell->gain() < 0 && $view=="allLosses") {
				$count++;
			} elseif ( $view=="all") {
				$count++;
			}
		}
		return (int)$count;
	}



	public function getSellCollections($sell,$panel_number)
	{
		return array(
			'id' 								=> $sell->Id,
			'ticker_id'					=> $sell->TickerId,
			'trade_detail_id'		=> $sell->getDetailsId(),
			'ticker_name' 			=> $sell->getTickerCode(),
			'date_entry' 				=> $sell->BuyDate,
			'date_exit' 				=> $sell->SellDate,
			'details_content' 	=> $sell->getDetailsContent(),
			'entry' 						=> $sell->BuyAmount,
			'exit' 							=> $sell->SellAmount,
			'gain' 							=> $sell->gain(),
			'images' 						=> $sell->getDetailsImages(),
			'likes' 						=> $sell->getDetailsLikes($sell->UserId),
			'comments'					=> $sell->getDetailsComments(),
			'owner_id'					=> $sell->UserId,
			'panel_number' 			=> $panel_number,
			'percent_gain' 			=> $sell->getPercentGain(),
			'volume' 						=> $sell->Volume
		);
	}




	public function update_details(Request $request) {
		$detailsId 	= $request->trade_detail_id;
		$sellId     = $request->id;
		$content 	= $request->content;
		$images 	= $request->images;
		$data = array(
			'SellId' => $sellId,
			'Content' => $content,
			'updated_at' => $this->getDateTimeNow()
		);
		if ($this->detailsExists($sellId)) {
			trade_details::updateBySell($sellId,$data);
		} else {
			trade_details::insert($data);
		}
		trade_details_images::where('SellId',$sellId)->delete();
		if ( count($images) > 0 ) {
			for ($i = 0; $i < count($images); $i++ ) {
				$imgId = $images[$i]['Id'];
				if (trade_details_images::isNew($imgId)) {
					trade_details_images::insert([
						'SellId' => $sellId,
						'Image'=>$images[$i]['Image'],
						'isNewUpload'=>1]);
				}
			}
		}

		return success("Trade Details has been updated") ;
	}




	private function detailsExists($sellId) {
		$tradeDetails = trade_details::where('SellId',$sellId)->first();
		return (!$tradeDetails) ? false : true;
	}


	public function show($sellId,$panel_number) {
		$sell = Sell::find($sellId);
		$api =  array(
			'id' 				=> $sell->Id,
			'ticker_id'			=> $sell->TickerId,
			'ticker_name' 		=> $sell->getTickerCode(),
			'trade_detail_id'	=> $sell->getDetailsId(),
			'date_entry' 		=> $sell->BuyDate,
			'date_exit' 		=> $sell->SellDate,
			'details_content' 	=> $sell->getDetailsContent(),
			'entry' 			=> $sell->BuyAmount,
			'exit' 				=> $sell->SellAmount,
			'gain' 				=> $sell->gain(),
			'images' 			=> $sell->getDetailsImages(),
			'owner_id'			=> $sell->UserId,
			'panel_number' 		=> $panel_number,
			'percent_gain' 		=> $sell->getPercentGain(),
			'volume' 			=> $sell->Volume
		);
		return $api;
	}










































































		//    public function GetTraideDetails($userId){
		//        $sell = sell::select(DB::raw('*'))
		//                                ->where('UserId','=',$userId)
		//                                ->where('deleted','=','0')
		//                                ->get();
		//        $returnData = array();
		//        foreach($sell as $row) {
		//            $trade_details = trade_details::select(DB::raw('*'))
		//                                            ->where('SellId','=',$row->Id)
		//                                            ->where('deleted','=','0')
		//                                            ->get();
		//            $trade_details_comments = trade_details_comments::select(DB::raw('*'))
		//                                            ->where('SellId','=',$row->Id)
		//                                            ->where('deleted','=','0')
		//                                            ->get();
		//            $trade_details_images = trade_details_images::select(DB::raw('*'))
		//                                            ->where('SellId','=',$row->Id)
		// 										->get();
		// 		$trade_details_likes =  trade_details_likes::select(DB::raw('*'))
		//                                            ->where('SellId','=',$row->Id)
		// 										->get();
		//            $returnData[] = array('Volume'=>$row->Volume,
		//                                  'SellDate'=>$row->SellDate,
		//                                  'SellAmount'=>$row->SellAmount,
		// 							  'BuyDate'=>$row->BuyDate,
		// 							  'BuyAmount'=>$row->BuyAmount,
		// 							  'content'=>'',
		// 							  'sell_id'=>$row->Id,
		// 							  'images'=>$trade_details_images,
		// 							  'comments'=>$trade_details_comments,
		// 							   'count_likes'=>$trade_details_likes
		// 							);
		//            // $row['sell']['Comment'] =$trade_details_comments->Comment;
		//            // $returnData[]=array('Content'=>$trade_details,'test'=>'123');
		//        }
		// return $returnData;
		//    }
		//    public function binance(){
		//        $history = $this->trades("BNBBTC");
		//        return  $history;
		// }
		// //bdindo
		// public $btc_value = 0.00;
		// protected $base = "https://api.binance.com/api/", $api_key = 'mJcRygFHRZSbtqpZRdDoJVrPCox220mESlPuJ36LPamzTNmGX57PxG4s43ho6Mqt', $api_secret = 'S452ExYLSFjvJYDjbB6UlJTJ78XKJMniCa3LRQLXWOgqMT7nFUoDLEFmmdoiuluc';
		// ///binance
		// public function history( $symbol,  $limit = 500,  $fromTradeId = 1 ) {
		// 	return $this->signedRequest( "v3/myTrades", "GET", [
		// 		  "symbol" => $symbol,
		// 		  "limit" => $limit,
		// 		  "fromId" => $fromTradeId
		// 	], true );
		//  }
		//    public function ping() {
		// 	return $this->request("v1/ping");
		// }
		// public function time() {
		// 	return $this->request("v1/time");
		// }
		// public function exchangeInfo() {
		// 	return $this->request("v1/exchangeInfo");
		// }
		// public function buy_test($symbol, $quantity, $price, $type = "LIMIT", $flags = []) {
		// 	return $this->order_test("BUY", $symbol, $quantity, $price, $type, $flags);
		// }
		// public function sell_test($symbol, $quantity, $price, $type = "LIMIT", $flags = []) {
		// 	return $this->order_test("SELL", $symbol, $quantity, $price, $type, $flags);
		// }
		// public function buy($symbol, $quantity, $price, $type = "LIMIT", $flags = []) {
		// 	return $this->order("BUY", $symbol, $quantity, $price, $type, $flags);
		// }
		// public function sell($symbol, $quantity, $price, $type = "LIMIT", $flags = []) {
		// 	return $this->order("SELL", $symbol, $quantity, $price, $type, $flags);
		// }
		// public function cancel($symbol, $orderid) {
		// 	return $this->signedRequest("v3/order",["symbol"=>$symbol, "orderId"=>$orderid], "DELETE");
		// }
		// public function orderStatus($symbol, $orderid) {
		// 	return $this->signedRequest("v3/order",["symbol"=>$symbol, "orderId"=>$orderid]);
		// }
		// public function openOrders($symbol) {
		// 	return $this->signedRequest("v3/openOrders",["symbol"=>$symbol]);
		// }
		// public function orders($symbol, $limit = 500) {
		// 	return $this->signedRequest("v3/allOrders",["symbol"=>$symbol, "limit"=>$limit]);
		// }
		// public function trades($symbol) {
		// 	return $this->signedRequest("v3/myTrades",["symbol"=>$symbol]);
		// }
		// public function prices() {
		// 	return $this->priceData($this->request("v1/ticker/allPrices"));
		// }
		// public function bookPrices() {
		// 	return $this->bookPriceData($this->request("v1/ticker/allBookTickers"));
		// }
		// public function account() {
		// 	return $this->signedRequest("v3/account");
		// }
		// public function depth($symbol) {
		// 	return $this->request("v1/depth",["symbol"=>$symbol]);
		// }
		// public function balances($priceData = false) {
		// 	return $this->balanceData($this->signedRequest("v3/account"),$priceData);
		// }
		// public function prevDay($symbol) {
		// 	return $this->request("v1/ticker/24hr", ["symbol"=>$symbol]);
		// }
		// private function request($url, $params = [], $method = "GET") {
		// 	$opt = [
		// 		"http" => [
		// 			"method" => $method,
		// 			"header" => "User-Agent: Mozilla/4.0 (compatible; PHP Binance API)\r\n"
		// 		]
		// 	];
		// 	$context = stream_context_create($opt);
		// 	$query = http_build_query($params, '', '&');
		// 	return file_get_contents($this->base.$url.'?'.$query, false, $context);
		// }
		// private function signedRequest($url, $params = [], $method = "GET") {
		// 	$params['timestamp'] = number_format(microtime(true)*1000,0,'.','');
		// 	$query = http_build_query($params, '', '&');
		// 	$signature = hash_hmac('sha256', $query, $this->api_secret);
		// 	$opt = [
		// 		"http" => [
		// 			"method" => $method,
		// 			"ignore_errors" => true,
		// 			"header" => "User-Agent: Mozilla/4.0 (compatible; PHP Binance API)\r\nX-MBX-APIKEY: {$this->api_key}\r\nContent-type: application/x-www-form-urlencoded\r\n"
		// 		]
		// 	];
		// 	if ( $method == 'GET' ) {
		// 		// parameters encoded as query string in URL
		// 		$endpoint = "{$this->base}{$url}?{$query}&signature={$signature}";
		// 	} else {
		// 		// parameters encoded as POST data (in $context)
		// 		$endpoint = "{$this->base}{$url}";
		// 		$postdata = "{$query}&signature={$signature}";
		// 		$opt['http']['content'] = $postdata;
		// 	}
		// 	$context = stream_context_create($opt);
		// 	return file_get_contents($endpoint, false, $context);
		// }
		// private function order_test($side, $symbol, $quantity, $price, $type = "LIMIT", $flags = []) {
		// 	$opt = [
		// 		"symbol" => $symbol,
		// 		"side" => $side,
		// 		"type" => $type,
		// 		"quantity" => $quantity,
		// 		"recvWindow" => 60000
		// 	];
		// 	if ( $type == "LIMIT" ) {
		// 		$opt["price"] = $price;
		// 		$opt["timeInForce"] = "GTC";
		// 	}
		// 	// allow additional options passed through $flags
		// 	if ( isset($flags['recvWindow']) ) $opt['recvWindow'] = $flags['recvWindow'];
		// 	if ( isset($flags['timeInForce']) ) $opt['timeInForce'] = $flags['timeInForce'];
		// 	if ( isset($flags['stopPrice']) ) $opt['stopPrice'] = $flags['stopPrice'];
		// 	if ( isset($flags['icebergQty']) ) $opt['icebergQty'] = $flags['icebergQty'];
		// 	return $this->signedRequest("v3/order/test", $opt, "POST");
		// }
		// private function order($side, $symbol, $quantity, $price, $type = "LIMIT", $flags = []) {
		// 	$opt = [
		// 		"symbol" => $symbol,
		// 		"side" => $side,
		// 		"type" => $type,
		// 		"quantity" => $quantity,
		// 		"recvWindow" => 60000
		// 	];
		// 	if ( $type == "LIMIT" ) {
		// 		$opt["price"] = $price;
		// 		$opt["timeInForce"] = "GTC";
		// 	}
		// 	// allow additional options passed through $flags
		// 	if ( isset($flags['recvWindow']) ) $opt["recvWindow"] = $flags['recvWindow'];
		// 	if ( isset($flags['timeInForce']) ) $opt["timeInForce"] = $flags['timeInForce'];
		// 	if ( isset($flags['stopPrice']) ) $opt['stopPrice'] = $flags['stopPrice'];
		// 	if ( isset($flags['icebergQty']) ) $opt['icebergQty'] = $flags['icebergQty'];
		// 	return $this->signedRequest("v3/order", $opt, "POST");
		// }
		// //1m,3m,5m,15m,30m,1h,2h,4h,6h,8h,12h,1d,3d,1w,1M
		// public function candlesticks($symbol, $interval = "5m") {
		// 	return $this->request("v1/klines",["symbol"=>$symbol, "interval"=>$interval]);
		// }
		// private function balanceData($array, $priceData = false) {
		// 	if ( $priceData ) $btc_value = 0.00;
		// 	$balances = [];
		// 	foreach ( $array['balances'] as $obj ) {
		// 		$asset = $obj['asset'];
		// 		$balances[$asset] = ["available"=>$obj['free'], "onOrder"=>$obj['locked'], "btcValue"=>0.00000000];
		// 		if ( $priceData ) {
		// 			if ( $obj['free'] < 0.00000001 ) continue;
		// 			if ( $asset == 'BTC' ) {
		// 				$balances[$asset]['btcValue'] = $obj['free'];
		// 				$btc_value+= $obj['free'];
		// 				continue;
		// 			}
		// 			$btcValue = number_format($obj['free'] * $priceData[$asset.'BTC'],8,'.','');
		// 			$balances[$asset]['btcValue'] = $btcValue;
		// 			$btc_value+= $btcValue;
		// 		}
		// 	}
		// 	if ( $priceData ) {
		// 		uasort($balances, function($a, $b) { return $a['btcValue'] < $b['btcValue']; });
		// 		$this->btc_value = $btc_value;
		// 	}
		// 	return $balances;
		// }
		// private function bookPriceData($array) {
		// 	$bookprices = [];
		// 	foreach ( $array as $obj ) {
		// 		$bookprices[$obj['symbol']] = [
		// 			"bid"=>$obj['bidPrice'],
		// 			"bids"=>$obj['bidQty'],
		// 			"ask"=>$obj['askPrice'],
		// 			"asks"=>$obj['askQty']
		// 		];
		// 	}
		// 	return $bookprices;
		// }
		// private function priceData($array) {
		// 	$prices = [];
		// 	foreach ( $array as $obj ) {
		// 		$prices[$obj['symbol']] = $obj['price'];
		// 	}
		// 	return $prices;
		//    }
}
