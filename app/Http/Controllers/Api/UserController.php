<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;

use App\UsersModel as User;
use App\UsersTradingStyleModel as TradingStyle;
use App\PrivacySettingsModel as Privacy;


class UserController extends BaseController
{

	public function GetUserInfobyUsername( Request $get ) {
		$result = array();
		$user = User::find_by_username($get->username);
		if ($user) {
			$result = array(
				'isExist' => true,
				'Firstname' => $user->getFirstname(),
				'Lastname' =>  $user->getLastname(),
				'CompleteName' => $user->completeName(),
				'Image' => $user->getImage(),
				'TradeStyle' => $user->getTradingStyle(),
				'AboutMe' => $user->getAboutMe(),
				'Username' => $user->getUsername(),
				'NumberOfFollowers' => $user->countFollowers(),
				'NumberOfFollowing' => $user->countFollowing()
			);
		} else {
			$result = array('isExist' => false);
		}

		return $result;

	}



}
