<?php
namespace App\Http\Controllers\API;
use Auth;
use App\User;
use App\_modules;
use App\_user_maintitle;
use App\users_information;
use App\sell;
use App\watchlist;
use App\tickers;
use App\buy;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use Illuminate\Support\Facades\DB;

class RoomController extends BaseController
{
    public function CreateRoom($data)
    {
        $RoomData = json_decode($data);
       

        if($RoomData->cmd == 'create'){
            $data1=array('room_name'=>$RoomData->room_name,
            "owner"=> $RoomData->owner ,
            "status"=>$RoomData->status,
            "key"=>$RoomData->key,
            "id"=>$RoomData->id,
            "images"=>$RoomData->images);

            DB::table('room')->insert($data1);
            
            $RoomList = room::select(DB::raw("*"))
            ->where('deleted','=','0')
            ->get();
            return $RoomList;
        }else{
            $RoomList = room::select(DB::raw("*"))
            ->where('deleted','=','0')
            ->get();
            return $RoomList;
        }
       
        
      
    }

    public function ApproveRequest($data){
        // $room_users = sell::select(DB::raw("*"))
        // ->where('UserId','=',$data)
        // ->where('deleted','=','0')
        // ->orderBy('SellDate')
        // ->get();
        $RoomUsers = json_decode($data);
        $Users=array('owner'=>$RoomUsers->owner,
                     'room_name'=>$RoomUsers->room_name,
                     'user_id'=>$RoomUsers->UserId,
                    'user_name'=>$RoomUsers->name );
        DB::table('room_users')->insert($Users);
        
        $RoomList = room::select(DB::raw("*"))
                            ->where('deleted','=','0')
                            ->get();
        return $data;
    }


}