<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use App\UsersModel as _User;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\PrivacySettingsModel;
use App\users_information;
use App\users_tradingstyle;
use App\users_broker;
use App\users_links;
use App\users_information as UserInfo;
class RegisterController extends BaseController
{
  public $successStatus = 200;
  /**
  * Register api
  *
  * @return \Illuminate\Http\Response
  */


  public function register(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'Firstname'=> 'required',
      'Lastname'=> 'required',
      'email' => 'required|email',
      'password' => 'required',
      'c_password' => 'required|same:password',
    ]);
    if($validator->fails()){
      return $this->sendError('Validation Error.', $validator->errors());
    }
    //* input for new registration*//
    $input = $request->all();
    $input['AccountSetUp'] = '1';
    $input['password'] =  hash('SHA256',$input['password']);
    $user = User::create($input);
    $users_information = array('UserId'=>$user->id,'Image'=>'temp.jpg','Address'=>'No Data', 'AboutMe'=>'No Data' );
    $users_tradingstyle = array('StyleId'=>'','UserId'=>$user->id);
    $users_broker = array('BrokerId'=>'0','UserId'=>$user->id);
    $users_links = array('UserId'=>$user->id,'Facebook'=>'','Twitter'=>'','LinkedIn'=>'');
    users_tradingstyle::create($users_information);//blank data for trading style
    users_broker::create($users_broker);// blank data for user broker
    users_information::create($users_information);
    $success['token'] =  $user->createToken('pltrades')->accessToken;
    $success['name'] =  $user;
    $success['users_information'] =  $users_information;
    return $success;
  }


  public function login() {
    $users = User::where('email', '=',request('email'))->where('password','=',hash('sha256',request('password')))->first();
    if (empty($users)) {
      return response()->json(['error'=> 'Unauthorized'], 401);
    }

    if(Auth::loginUsingId($users->id)){
      $user = Auth::user();
      $success['token'] =  $user->createToken('pltrades')->accessToken;
      $GetUser= User::select('id','Firstname','Username','Lastname','AccountSetUp')->where('users.id',$users->id)->first();
      $userInfo = users_information::select('Image')->where('UserId',$users->id)->first();
      $userlist[] = array(
        'name'=>$GetUser->Firstname . ' ' . $GetUser->Lastname,
        'id'=>$users->id,
        // 'img'=>'../assets/img/'. $userInfo->Image,
        'username' => $GetUser->Username,
        'img'=> UserInfo::getImage($users->id),
        'AccountSetUp'=>$GetUser->AccountSetUp
      );
      $data = array(
        'success' => $success,
        'userdata'=>$userlist
      );
      return $data;
    }

    return $users;
  }


  public function getUser() {
    $user = Auth::user();
    return response()->json(['success' => $user], $this->successStatus);
  }



  public function AccountSetupRegistration(Request $request) {
    $userId = $request->myId;

    $profile = $request->profile;

    // //Main user
    $user = $this->CreateUserMainInfo($profile['personal'],$userId);
    // //User Information
    // //Links
    // //Registration
    // //trading Style
    // //Privacy
    //  // $createPrivacy =  $this->CreatePrivacy($request->privacySettings,$userId);
    $response = array('status' =>  $user );
    return $response;

  }




  public function CreateUpdateUserInfo($info,$userId) {

    $user = _User::find_by_id($userId);
    $user->setFirstname($info['firstname']);
    $user->setLastname($info['lastname']);
    $user->setEmail($info['email']);

    $userInfo = Userinfo::findByColumn('UserId',$userId);
    $userInfo->setImage($info['user_image']);
    $userInfo->setAddress($info['address']);
    $userInfo->setAboutMe($info['aboutMe']);
    return $user->update_users() && $userInfo->update_info();
  }


  public function CreatePrivacy($privacy,$userId) {
    $PrivacySettings = new PrivacySettingsModel();
    $PrivacySettings->setUserId($userId);
    $PrivacySettings->setTradePerformance($privacy['tradePerformance']);
    $PrivacySettings->setTradeDetails($privacy['tradeDetails']);
    $PrivacySettings->setAccount($privacy['accountPrivacy']);
    $PrivacySettings->setEmail($privacy['emailAddress']);
    $PrivacySettings->setAddress($privacy['address']);
    $PrivacySettings->setTradeStats($privacy['tradeStats']);
    return $PrivacySettings->create_privacy();
  }


  public function CreateUpdateTradesInformation() {

  }





















}
