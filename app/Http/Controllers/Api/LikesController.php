<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\trade_details_likes;
use App\StrategyRulesLikesModel as StratRulesLikes;
use App\Http\Controllers\API\BaseController as BaseController;

class LikesController extends BaseController
{

	public function getLike(Request $request) {
		$table =  $request->data;
		$userId = $request->userid;
		$refId  = $request->data_id;

		switch ($table) {
			case 'trade-details':
			$this->trade_details_like($userId,$refId);
			break;
			case 'strategy-rules':
			$this->strategy_rules_like($userId,$refId);
			break;
		}
	}


	public function strategy_rules_like($userId,$stratId) {
		$stratRules = new StratRulesLikes();
		$stratRules->UserId = $userId;
		$stratRules->StrategyRulesId = $stratId;
		if ($stratRules->userAlreadyLike()) {
			$stratRules->deleteLike();
			$status = 'unlike';
		}else{
			$stratRules->insertLike();
			$status = 'like';
		}
		return $this->sendResponse([], $status);
	}

	public function trade_details_like($userId,$sellId) {
		$detailsLike = new trade_details_likes();
		$detailsLike->UserId =  $userId;
		$detailsLike->SellId =  $sellId;
		if ($detailsLike->userAlreadyLike()) {
			$detailsLike->deleteLike();
			$status = 'unlike';
		} else {
			$detailsLike->insertLike();
			$status = 'like';
		}
		return $this->sendResponse([], $status);
	}

}
