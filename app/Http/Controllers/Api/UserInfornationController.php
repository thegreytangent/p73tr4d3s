<?php


namespace App\Http\Controllers\API;
use Auth;
use App\User;
use App\UsersModel as _User;
use App\_modules;
use App\_user_maintitle;
use App\users_information;
use App\sell;
use App\watchlist;
use App\tickers;
use App\buy;
use App\watchlist_comments;
use App\watchlist_likes;
use App\follows;
use App\trading_style;
use App\users_tradingstyle;
use App\users_links;
use App\brokers;
use App\users_broker;
use App\PrivacySettingsModel as Privacy;



use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use Illuminate\Support\Facades\DB;


class UserInfornationController extends BaseController
{
  public function Userinfo(){
    $user = Auth::user();
    $getuser= User::select('users.id',
    'users_information.Image',
    'users.Firstname',
    'users.Lastname',
    'users.email',
    'users_information.AboutMe',
    'users_information.Address')
    ->join('users_information','users_information.UserId','=','users.id')
    ->where('users.id',  $user->id)->first();
    $followers = follows::select('UserId')->where('UserId',request('UserId'))->count();
    $following = follows::select('FollowUser')->where('FollowUser',request('UserId'))->count();
    $style = users_tradingstyle::select("*")->where('UserId',$getuser->id)->first();
    $traders_type = trading_style::select("*")->where('id',$style->StyleId)->first();
    $users_links = users_links::select("*")->where('UserId', $user->id)->first();
    $users_broker = users_broker::select("*")->where('UserId',$user->id)->first();
    $brokers = brokers::select("Name as name, Id as id ")->where('Id', $users_broker->BrokerId)->get();

    $userinformation = array();

    ///userinformation
    $list['user_id'] = $user->id;
    $list['name'] =$getuser->Firstname . ' ' . $getuser->Lastname;
    $list['user_image'] ='../assets/img/'.$getuser->Image;

    $list['tradingStyle'] =  $traders_type->Style;
    $list['numbersFollowers'] = $followers;
    $list['numbersFollowing'] = $following;
    $list['aboutMe'] = $getuser->AboutMe;
    $list['address'] = $getuser->Address;
    $list['firstname'] = $getuser->Firstname;
    $list['lastname'] = $getuser->Lastname;
    $list['email'] = $getuser->email;

    //user link
    $link['facebook']= $users_links->Facebook;
    $link['linkedin'] = $users_links->LinkedIn;
    $link['twitter'] = $users_links->Twitter;

    //borker
    $Broker['brokers'] = $brokers;


    $userinformation['myId'] =  $user->id;
    $userinformation['profile']['links'] = $link;
    $userinformation['profile']['personal'] = $list;
    $userinformation['profile']['tradesInformation'] =  $Broker;
    $userinformation['profile']['tradesInformation']['styleOfTrading'] =   $traders_type->Id;




    return $userinformation;


  }

  public function UserProfile(Request $request){
    $user = Auth::user();
    $getuser= User::select('users.id',
    'users_information.Image',
    'users.Firstname',
    'users.Lastname',
    'users.email',
    'users_information.AboutMe',
    'users_information.Address')
    ->join('users_information','users_information.UserId','=','users.id')
    ->where('users.id', request('UserId'))->first();
    $followers = follows::select('UserId')->where('UserId',request('UserId'))->count();
    $following = follows::select('FollowUser')->where('FollowUser',request('UserId'))->count();
    // $style = users_tradingstyle::select("*")->where('UserId',$getuser->id)->first();
    // $traders_type = trading_style::select("*")->where('id',$style->StyleId)->first();
    $users_links = users_links::select("*")->where('UserId', $request->UserId)->first();
    // $users_broker = users_broker::select("*")->where('UserId',$user->id)->first();
    // $brokers = brokers::select("Name as name, Id as id ")->where('Id', $users_broker->BrokerId)->get();
    $userinformation = array();

    $find_user = _User::find_by_id($request->UserId);


    ///userinformation
    $list['user_id'] = $request->UserId;
    $list['name'] =$getuser->Firstname . ' ' . $getuser->Lastname;
    // $list['user_image'] ='../assets/img/'.$getuser->Image;
    $list['user_image'] = $find_user->getImage();
    // $list['tradingStyle'] =  $traders_type->Style;
    $list['tradingStyle'] =  $find_user->getTradingStyle();
    $list['numbersFollowers'] = $followers;
    $list['numbersFollowing'] = $following;
    $list['aboutMe'] = $getuser->AboutMe;
    $list['address'] = $getuser->Address;
    $list['firstname'] = $getuser->Firstname;
    $list['lastname'] = $getuser->Lastname;
    $list['email'] = $getuser->email;

    //Privacy
    $privacy = Privacy::find_by_user($request->UserId);
    $list['privacy'] = array(
      'account'           => $privacy->getAccount(),
      'address'           => $privacy->getAddress(),
      'email'             => $privacy->getEmail(),
      'trade_details'     => $privacy->getTradeDetails(),
      'trade_performance' => $privacy->getTradePerformance(),
      'trade_stats'       => $privacy->getTradeStats(),
    );
  
    $list['follow_status'] = follows::follow_status(Auth::user()->id, $request->UserId);


    return $list;


  }


  //search engine
  // BookingDates::where('email', Input::get('email'))
  // ->orWhere('name', 'like', '%' . Input::get('name') . '%')->get();
  public function UserInfoUpdate(Request $request){

    return $request->all();
  }





}
