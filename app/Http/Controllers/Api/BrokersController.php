<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\brokers;
use App\Http\Controllers\API\BaseController as BaseController;

class BrokersController extends BaseController
{

	public function index() {
		return brokers::getAll();
	}
}