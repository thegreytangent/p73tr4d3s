<?php
namespace App\Http\Controllers\API;

use App\watchlist as Watchlist;
use App\WatchListBookmarksModel as WatchListBookmarks;
use App\tickers  as Ticker;
use App\StrategyRulesBookmarkModel as StrategyRulesBookmark;
use App\strategy_rules as StrategyRules;
use App\UsersModel as _User;


class BookmarksController extends BaseController {



	public function getBookmarks($userId) {
		$bookmarks = array();
		$getBookmarks = array();
		$getStratBookmarks = StrategyRulesBookmark::findByColumnArray('UserId',$userId);
		if ($getStratBookmarks) {
			foreach ($getStratBookmarks as $strategy)
			{
				$getStrat = StrategyRules::find_by_id($strategy->getStrategyRulesId());
				$trader = _User::find_by_id($getStrat->getUserId());
				$bookmarks['Id'][] = $strategy->getId();
				$bookmarks['traders'][] = $trader->getId();
				$bookmarks['type'][] = 'Strategy Rules';
				$bookmarks['ticker'][] = $getStrat->getTitle();
				$bookmarks['message'][] = $getStrat->getContentLimitBy(5) ;
				$bookmarks['tradersName'][] = $trader->completeName();
			}
		}

		foreach (WatchListBookmarks::findByColumnArray('UserId', $userId) as $watchlist){
			$getWatchlist = Watchlist::find_by_id($watchlist->getWatchlistId());
			$trader = _User::find_by_id($getWatchlist->getUserId());
			$bookmarks['Id'][] = $watchlist->getWatchlistId();
			$bookmarks['traders'][] = $trader->getId();
			$bookmarks['type'][] = 'Watchlist';
			$bookmarks['ticker'][] = $getWatchlist->ticker();
			$bookmarks['message'][] = $getWatchlist->getContentLimitBy(15);
			$bookmarks['tradersName'][] = $trader->completeName();
		}

		if ($bookmarks) {
			for ($x = 0; $x < count($bookmarks['Id']); $x++) {
				$getBookmarks[] = array(
					'tradersName' => $bookmarks['tradersName'][$x],
					'type' => $bookmarks['type'][$x],
					'ticker' => $bookmarks['ticker'][$x],
					'message' => $bookmarks['message'][$x]
				);
			}
		}

		return $getBookmarks;
	}



	public function getDashboardWatchlist($userId) {
		$watchlist = array();
		$getWatchlist = Watchlist::dashboard_watchlist(674);
		if ($getWatchlist) {
			foreach ($getWatchlist as $list) {
				$watchlist[] = array(
					'id' => $list->Id,
					'ticker_name' => Ticker::getTickerCode($list->TickerId),
					'content' =>  $list->getContent()
				);
			}
		}
		return $watchlist;
	}



}
