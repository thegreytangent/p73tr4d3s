<?php
namespace App\Http\Controllers\API;
use Auth;
use App\User;
use App\UsersModel as _User;
use App\_modules;
use App\_user_maintitle;
use App\users_information;
use App\sell;
use App\watchlist;
use App\tickers;
use App\buy;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use Illuminate\Support\Facades\DB;


class TradePeformanceController extends BaseController
{
  protected static
  $brokerCommission = 0.0025,
  $vat = 0.12,
  $securityClearing = 0.0001,
  $salesTax = 0.006,
  $pseFee = 0.00005,
  $Result;


  public function GetTicker(){
    $GetTicker = tickers::select('Id as id','Code as value')->get();
    return $GetTicker;
  }


  public function GetBuyRecord($buy){
    $data = json_decode($buy);
    $buy = buy::where('UserId','=', $data->UserId)->where('deleted','=','0')
    ->orderBy('BuyDate','desc')->get();
    return $buy;
  }


  public function GetBuyTotal(){ //volume = share // entry = price
    // $data1 = json_decode($data);
    $buys = array();
    $buy = buy::select(DB::raw('*','TransactionId','Id'))
    ->where('UserId','=',request('UserId'))
    //   ->groupby('TransactionId','TickerId')
    ->orderBy('BuyDate','desc')->get();
    foreach($buy as $row) {
      $tickerDesc = tickers::select(DB::raw("Code"))->where('Id','=',$row->TickerId)->first();
      $price = $row->BuyAmount;
      $grossBuy = ($row->BuyVolume * $price);
      $commission = ( $grossBuy * 0.0025);//condition if lower than 20 '20'
      if($commission <= 20){
        $commissionOut = 20;
      }else{
        $commissionOut = ( $grossBuy * 0.0025);
      };
      $vat = ($commissionOut * 0.12);
      $PseTransFee = ($grossBuy * 0.00005); //PSE trans fee
      $Sccp = ($grossBuy * 0.0001);
      $TotalBuyFee = ($grossBuy + $commissionOut + $vat + $PseTransFee + $Sccp );
      $AverageEntry = ( $price / $TotalBuyFee);
      $buys[] =array(
        'TransactionId'=>$row->TransactionId,
        'Ticker'=>$tickerDesc->Code,
        'TickerId'=>$row->TickerId,
        'Volume'=>$row->BuyVolume,
        'Price'=>number_format((float)$row->BuyAmount, 2, '.', ''),
        'Vat'=>$vat,
        'Commission'=>$commission,
        'PseTransFee'=>$PseTransFee,
        'Sccp'=>$Sccp,
        'TotalBuyFee'=>$TotalBuyFee,
        'AverageEntry'=>$AverageEntry
      );//details
    }
    $result = [];
    $data1 = [];
    $totals = array();
    if (!$buys) {
      $ReturnDta = array();
    }else {
      foreach($buys as $item) {
        if(!array_key_exists($item['Ticker'], $totals)) {
          // We haven't come across this name yet, so make a placeholder with 0's
          $totals[$item['Ticker']] = array(
            'TransactionId' => $item['TransactionId'],
            'counter'       =>0,
            'Ticker'        => $item['Ticker'],
            'TickerId'      => $item['TickerId'],
            'Volume'        =>0,
            'Price'         =>0,
            'PseTransFee'   => 0,
            'Sccp'          => 0,
            'TotalBuyFee'   => 0,
            'AverageEntry'  => 0,
          );
        }
        //    Increment the amounts for the specific name
        $totals[$item['Ticker']]['counter'] += 1;
        $totals[$item['Ticker']]['TransactionId'] = $item['TransactionId'];
        $totals[$item['Ticker']]['PseTransFee'] += $item['PseTransFee'];
        $totals[$item['Ticker']]['Sccp'] += $item['Sccp'];
        $totals[$item['Ticker']]['TotalBuyFee'] += number_format($item['TotalBuyFee'], 2, '.', '') ;
        $totals[$item['Ticker']]['Volume'] = (($item['Volume'] + $totals[$item['Ticker']]['Volume']));
        if($totals[$item['Ticker']]['counter']>=2){
          $totals[$item['Ticker']]['Price'] = number_format((float)( ( $totals[$item['Ticker']]['Volume']) )/( $totals[$item['Ticker']]['TotalBuyFee']), 2, '.', '');
          $totals[$item['Ticker']]['AverageEntry'] =number_format((float) ( ( $totals[$item['Ticker']]['Volume'])/ ( $totals[$item['Ticker']]['TotalBuyFee'])), 2, '.', '');
        }else{
          $totals[$item['Ticker']]['Price'] += number_format((float)$item['Price'], 2, '.', '');
          $totals[$item['Ticker']]['AverageEntry'] = number_format((float)(( $totals[$item['Ticker']]['Volume']) )/ ( $totals[$item['Ticker']]['TotalBuyFee']), 2, '.', '') ;
        }
      }
      $resultFormated = [];
      $AllData = [];
      $AllData2 = [];
      $selected = [];
      $ReturnDta = [];
      foreach ($totals as $key => $value) {
        $resultFormated = array('ticker' => $key);
        $AllData[] =$value;
      }
      foreach($AllData as $row){
        $compute_volume = $row['Volume'] - $this->ValueAmount($row['TransactionId']);
        if ( $row['Volume'] != 0 ) {
          $ReturnDta[] = array(
            'AverageEntry'=>round($row['AverageEntry'],2),
            'Price'=>round($row['Price'],2),
            'PseTransFee'=>$row['PseTransFee'],
            'Sccp'=>$row['Sccp'],
            'Ticker'=>$row['Ticker'],
            'TickerId'=>$row['TickerId'],
            'TotalBuyFee'=>round($row['TotalBuyFee'], 2),
            'TransactionId'=>$row['TransactionId'],
            'Volume'=> $row['Volume'],
            'counter'=>$row['counter']
          );
        }
      }
    } //end of buys
    return $ReturnDta ;
    // return $ReturnDta ;
  }
  public function ValueAmount($trans){
    $sellDataCount =  sell::select(DB::raw("'TransactionId'"))
    ->where('TransactionId',$trans)
    ->where('deleted','=','0')
    ->count();
    if($sellDataCount>=1){
      $sellData =   sell::select(DB::raw("SUM(Volume) as Volume"))
      ->where('TransactionId','=',$trans)
      ->where('deleted','=','0')
      ->groupby('TransactionId','TickerId')
      ->first();
      return $sellData->Volume;
    }else{
      return $sellDataCount;
    }
  }
  //insert data for buy
  public function InsertBuy(Request $request){
    $UserTrans = sell::select(DB::raw("*"))->where('UserId',$request->UserID)->count();
    if(empty($UserTrans)){
      $UserTrans = 0;
    }
    if($request->cmd=='add'){
      $TransactionId = $request->TransactionId;
    }else{
      $TransactionId = date("Y") . $request->tickerId . $request->UserID . $UserTrans++;
    }
    // $TransactionId = date("Y") . $GetTickerId->Id . $request->UserID . $UserTrans;
    $date = date_create($request->formatted_date);
    $data=array('UserId'=>$request->UserID,
    "TickerId"=> $request->tickerId ,
    "BuyDate"=>date_format($date, 'Y-m-d H:i:s'),
    "BuyVolume"=>$request->share,
    "BuyAmount"=>$request->entry,
    "TransactionId"=>$TransactionId);
    buy::insert($data);
    $all= array('status'=>'OK');
    return $all;
  }
  public function getEntryDetail(){
    $getEntryDetails = buy::select(DB::raw("*"))->where('TransactionId',request('TransactionId'))->get();
    $alldeatails = array();
    $i= 1;
    foreach($getEntryDetails as $row){
      $tickerName = tickers::select(DB::raw("*"))->where('Id',$row['TickerId'])->first();
      $details['position'] = $i++;
      $details['Id'] = $row['Id'];
      $details['ticker'] = $tickerName->Code;
      $details['volume'] = $row['BuyVolume'];
      $details['exit'] = $row['BuyAmount'];
      $details['TransactionId'] = $row['TransactionId'];
      $alldeatails[] = $details;
    }
    return $alldeatails;
  }
  public function EditDelete(Request $request){
    if($request->cmd == 'delete'){
      DB::table('buy')->where('Id', '=',$request->Id)->delete();
      $all= array('status'=>'OK');
      return $all;
    }else
    if($request->cmd =='edit'){
      $date = date_create($request->date);
      DB::table('buy')
      ->where('Id', $request->Id)
      ->limit(1)
      ->update(array('BuyDate' => $date,'BuyVolume'=>$request->share,'BuyAmount'=>$request->entry));
      //   $all= array('status'=>'OK');
      return $request->Id;
    }
  }
  //insert data for sell
  public function InsertSell(Request $request){
    $UserTrans = buy::select(DB::raw("*"))->where('UserId',$request->UserID)->count();
    $TransactionId = date("Y") . $request->tickerId . $request->UserID . $UserTrans;
    $date = date_create($request->formatted_date);
    $data=array('UserId'=>$request->UserID,
    "TickerId"=>$request->tickerId,
    "SellDate"=>date_format($date, 'Y-m-d H:i:s'),
    "Volume"=>$request->share,
    "BuyAmount"=>$request->BuyAmount,
    "TransactionId"=>$request->TransactionId,
    "SellAmount"=>$request->entry,
    "BuyDate"=>NOW());
    // if($data=>'tickerId'==NULL){
    // }
    sell::insert($data);
    $all= array('status'=>'OK');
    return $all;
  }
  public function chartPerDay($data){
    $chart = json_decode($data);
    $sell = sell::select(DB::raw("sum(SellAmount) as SellAmount, SellDate"))
    ->where('UserId', '=',$data)
    ->where('deleted','=','0')
    ->groupBy('SellDate')
    ->get(1);
    $all = [];
    foreach($sell as $row){
      $all['data'][]= [ (strtotime($row->SellDate) * 1000) ,round($row->SellAmount,2) ];
      // $all['label'][]= date('d', strtotime($row->SellDate));
    }
    // [ (strtotime($row->SellDate) * 1000) ,round($row->SellAmount,2) ];
    return $all;
  }
  public function SellTable($data) {
    $all = array();
    $sell = sell::select(DB::raw("Id,BuyDate,BuyAmount,BuyAmount as AveEntry, SellDate as ExitDate, Volume, TickerId,SellAmount,TransactionId "))
    ->where('UserId','=',$data)
    ->where('deleted','=','0')
    ->orderBy('ExitDate')
    ->get();
    $SellTableData = [];
    $count = 1;
    $total = 0;
    foreach($sell as $row){
      $ExitDate = strtotime($row['ExitDate']);
      $BuyDate = strtotime($row['BuyDate']);
      // if($count==1){
      //   $ComputeGain = 0;
      // }else{
      //  $ComputeGain =$this->computeGain($row['SellAmount'],$row['BuyAmount'],$row['Volume']);
      // }
      $counter = $count++;
      $ComputeGain =$this->computeGain($row['SellAmount'],$row['BuyAmount'],$row['Volume']);
      $total =  $total + $ComputeGain;
      $GetTickerId =tickers::select("Code")->where('Id','=',$row['TickerId'])->first();
      $SellTableData[] = array(
        'position'=>$counter,
        'ticker'=>$GetTickerId->Code,
        'volume'=>abs(number_format($row['Volume'],2)),
        'AveEntry'=>abs(number_format($row['AveEntry'],2)),
        'exit'=>abs(number_format($row['SellAmount'],2)),
        'ExitDate'=>Date('m-d-y', $ExitDate),
        'EnterDate'=>Date('m-d-y', $BuyDate) ,
        'gain'=>round(abs($ComputeGain),2),
        'percent'=>round(abs($this->percentageGain($row['SellAmount'],$row['BuyAmount'],$row['Volume'])), 2),
        'total'=>number_format(abs(round($total,2)),2),
        'holding'=>$this->display_holdingPeriod($row['BuyDate'],$row['ExitDate']),
        'TransactionId'=>$row['TransactionId'],
        'Id'=>$row['Id'],
        'action'=>''
      );
      $chart[]= [ "date"=>$row['ExitDate'], "visits"=>number_format($total,2)];
      $all = array('chart'=>$chart,'SellTable'=>array_reverse($SellTableData));
    }
    $chart = [];
    // foreach($sell as $row2){
    //   $ComputeGain = number_format(abs($this->computeGain($row2['SellAmount'],$row2['BuyAmount'],$row2['Volume'])), 2);
    //   $total +=$ComputeGain;
    //   $per = $this->percentageGain($row2['SellAmount'],$row2['BuyAmount'],$row2['Volume']);
    //     $chart[]= [ (strtotime($row2['ExitDate']) * 1000) , $total ];
    //     // $all['label'][]= date('d', strtotime($row->SellDate));
    // }
    // $all = array('chart'=>$chart,'SellTable'=>array_reverse($SellTableData));
    return $all;
  }



  public function computeGain($SellAmount,$BuyAmount,$Volume)
  {
    return $this->computeNet($SellAmount,$BuyAmount,$Volume) - ($BuyAmount * $Volume);
  }
  public function computeNet($SellAmount,$BuyAmount,$Volume)
  {
    return $this->computeGross($SellAmount,$Volume) - $this->computeDeductions($SellAmount,$Volume);
  }
  public function computeDeductions($SellAmount,$Volume)
  {
    $deductions = array(
      $this->computeBrokerComission($SellAmount,$Volume),
      $this->computeVat($SellAmount,$Volume),
      $this->computeSecurityClearing($SellAmount,$Volume),
      $this->computeSalesTax($SellAmount,$Volume),
      $this->computePseFee($SellAmount,$Volume)
    );
    return array_sum($deductions);
  }
  private function computeBrokerComission($SellAmount,$Volume)
  {
    $compute = $this->computeGross($SellAmount,$Volume) * self::$brokerCommission;
    return ($compute < 20) ? 20 : $compute;
  }
  private function computeVat($SellAmount,$Volume)
  {
    return $this->computeBrokerComission($SellAmount,$Volume) * self::$vat;
  }
  private function computeSecurityClearing($SellAmount,$Volume)
  {
    return $this->computeGross($SellAmount,$Volume) * self::$securityClearing;
  }
  private function computeSalesTax($SellAmount,$Volume)
  {
    return $this->computeGross($SellAmount,$Volume) * self::$salesTax;
  }
  private function computePseFee($SellAmount,$Volume)
  {
    return $this->computeGross($SellAmount,$Volume) * self::$pseFee;
  }
  public function computeGross($SellAmount,$Volume)
  {
    return $Volume * $SellAmount;//need to manipulate the data
  }
  public function percentageGain($SellAmount,$BuyAmount,$Volume)
  {
    return ($this->computeNet($SellAmount,$BuyAmount,$Volume) - ($BuyAmount * $Volume)) / ($BuyAmount * $Volume) * 100;
  }
  public function calculateHoldingPeriod($BuyDate,$SellTime) {
    $startDate = strtotime($BuyDate);
    $endDate = strtotime($SellTime);
    if (empty($startDate) || ($startDate == "")) {
      $result = -1;
    } else {
      $datediff = $endDate - $startDate;
      $result =  floor($datediff / (60 * 60 * 24));
    }
    return $result;
  }
  public function display_holdingPeriod($BuyDate,$SellTime) {
    $holding =  $this->calculateHoldingPeriod($BuyDate,$SellTime);
    if ($holding == 0 ) {
      $display = 'Intra';
    }
    elseif ($holding == 1 ) {
      $display = $holding." day";
    }
    elseif ($holding < 0 ) {
      $display = "n/a";
    }
    else {
      $display = $holding . " days";
    }
    return $display;
  }
  public function GetDetailBuy($data){
    $buy = buy::select(DB::raw('*'))
    ->where('TransactionId','=',$data)
    ->where('deleted','=','0')
    ->orderBy('BuyDate','desc')->get();
    foreach($buy as $row) {
      // $tickerDesc = tickers::select(DB::raw("Code"))->where('Id','=',$row->TickerId)->first();
      $GetTickerId =tickers::select("Code")->where('Id','=',$row['TickerId'])->first();
      $price = number_format((float)$row['BuyAmount'], 2, '.', '');
      $grossBuy = ($row['BuyVolume'] * $price);
      $commission = ( $grossBuy * 0.0025);//condition if lower than 20 '20'
      if($commission <= 20){
        $commissionOut = 20;
      }else{
        $commissionOut = ( $grossBuy * 0.0025);
      };
      $vat = ($commissionOut * 0.12);
      $PseTransFee = ($grossBuy * 0.00005); //PSE trans fee
      $Sccp = ($grossBuy * 0.0001);
      $TotalBuyFee = ($grossBuy + $commissionOut + $vat + $PseTransFee + $Sccp );
      $AverageEntry = ( $TotalBuyFee / $price);
      $buys[] =array(
        'TransactionId'=>strval($row['TransactionId']),
        'Ticker'=>$GetTickerId->Code,
        'TickerId'=>$row['TickerId'],
        'Volume'=>$row['BuyVolume'],
        'Price'=>number_format((float)$row['BuyAmount'], 2, '.', ''),
        'Vat'=>$vat,
        'Commission'=>$commission,
        'PseTransFee'=>$PseTransFee,
        'Sccp'=>$Sccp,
        'TotalBuyFee'=>$TotalBuyFee,
        'AverageEntry'=>$AverageEntry,
        'EntryDate'=>date('F j, Y', strtotime($row['BuyDate'])),
        'Id'=>$row['Id']
      );//details
    }
    return $buys;
  }



  public function UpdateBuy($data){
    $buy = buy::select(DB::raw('*'))
    ->where('Id','=',$data)
    ->update(['deleted'=>'1']);
    return   $all= array('status'=>'OK');;
  }



  public function DeleteData($data){
    $dataToDelete = json_decode($data);
    if($dataToDelete->cmd =='sell'){
      $sell = sell::select(DB::raw('*'))
      ->where('Id','=',$dataToDelete->data)
      ->update(['deleted'=>'1']);
      return   $all= array('status'=>'OK');
    }else{
      $buy = buy::select(DB::raw('*'))
      ->where('TransactionId','=',$dataToDelete->data)
      ->update(['deleted'=>'1']);
      return   $all= array('status'=>'OK');
    }
  }







  public function getSellRecords(Request $get) {
    $user = _User::find_by_UsernameOrId($get->user);

    //Refactor in the future
    $result = array();

    $total = array();
    $percentage = array();
    $runningBalance = 0;
    $runningPercentage = 0;

    foreach ( sell::findbyUserOrderByDateAsc($user->getId()) as $trades) {
      $trade = $trades->computeGain();
      $runningBalance = $runningBalance + $trade;
      $total[] = $runningBalance;
      $percentage[] = $trades->percentGain();
    }

    $total = array_reverse($total);
    $percentage = array_reverse($percentage);

    $count = count(sell::findbyUserOrderByDateDesc($user->getId()));
    $balCounter = 0;
    $data = array();

    foreach ( sell::findbyUserOrderByDateDesc($user->getId()) as $trades ) {

      $result['trades'][] = array(
        'Id' => $trades->getId(),
        'AveEntry' => $trades->displayBuyAmount(),
        'EntryDate' =>$trades->entryDate(),
        'ExitDate' => $trades->getExitDate(),
        'exit' => $trades->getExit(),
        'TransactionId' => $trades->getTransactionId(),
        'gain' => $trades->computeGain(),
        'holding' => $trades->display_holdingPeriod(),
        'percent' => $percentage[$balCounter],
        'position' => $count,
        'action' => '',
        'ticker' => $trades->getTickerCode(),
        'total' => $total[$balCounter],
        'volume' => $trades->getVolume()
      );

      $balCounter++;
      $count--;
    }

    return $result;
  }







}
