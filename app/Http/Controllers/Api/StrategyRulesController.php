<?php


namespace App\Http\Controllers\API;
use Auth;
use App\User;
use App\_modules;
use App\_user_maintitle;
use App\users_information;
use App\sell;
use App\watchlist;
use App\tickers;
use App\buy;
use App\watchlist_comments;
use App\watchlist_likes;
use App\strategy_rules;
use App\strategy_rules_images;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use Illuminate\Support\Facades\DB;


class StrategyRulesController extends BaseController
{
    public function getStrategyRules(){
        $strategy_rules = strategy_rules::select( 'Id','UserId' ,'Content', 'Title','Date')->where('UserId',request('user_id'))->get();
        $data = array();
        foreach($strategy_rules as $row) {
            $images =  strategy_rules_images::select(DB::raw('CONCAT("Id") as id'),DB::raw('CONCAT("../assets/img/","",Images) as image'))->where('StrategyRulesId',$row['Id'])->get();


            $arr['comments']= '';
            $rr['content'] = $row['Content'];
            $rr['date'] = $row['Date'];
            $rr['images']=$images;
            $rr['likes']= '';
            $rr['title'] = $row['Title'];
            $rr['user_id'] = $row['UserId'];
            $data[] = $rr;
        }
        
        return $data;
    }



}