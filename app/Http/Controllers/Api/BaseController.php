<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Facades\DB;


class BaseController extends Controller
{


    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message)
    {
    	$response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];
        return response()->json($response, 200);
    }


    public static function getDateTimeNow() {
        return date("Y-m-d H:i:s");
    }

   


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
    	$response = [
            'success' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }


    //Update table by "Id"
    public static function update($id,$columnData) {
        $result = DB::table(static::$table)->where('Id', $id)->update($columnData);
        return ($result) ? TRUE : FALSE;
    }

    //delete specific column
    public static function delete_by($column,$id) {
        $result = DB::table(static::$table)->where($column,'=', $id)->delete();
        return ($result) ? TRUE : FALSE;
    }

    public static function create($columnData) {
        $result = DB::table(static::$table)->insert($columnData);
        return ($result) ? TRUE : FALSE;
    }

    public static function findByColumn($column,$id) {
         $result = DB::table(static::$table)->where($column,'=', $id);
         return ($result) ? $result->get() : FALSE;
    }

    













}