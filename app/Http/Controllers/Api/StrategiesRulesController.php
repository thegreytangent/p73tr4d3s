<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\UsersModel as User;
use App\StrategyRulesModel as StrategyRules;
use App\StrategyRulesImagesModel as StratImage;
use App\Http\Controllers\API\BaseController as BaseController;

class StrategiesRulesController extends BaseController {
	// Request $request


	public function createStrategiesRules(Request $request) {
		$StrategyRules = new StrategyRules();
		$StrategyRules->UserId 	= $request->user_id;
		$StrategyRules->Title 	= $request->title;

		if ($StrategyRules->create()) {
			$response = $this->sendResponse([$StrategyRules], 'Strategy Rules added successfully!');
		} else {
			$response = $this->sendError('Unable to create', $errorMessages = [$StrategyRules], $code = 404);
		}

		return $response;
	}



	public function updateStrategiesRules (Request $request) {
	$error = array();
	$id 		= $request->id;
	$content 	= $request->content;
	$images 	= $request->images;
	$StrategyRules = StrategyRules::find_by_id($id);
	if ($StrategyRules) {
		if ($content) {
			$StrategyRules->Content = $content;
			$error['content'] = (!$StrategyRules->updateContent()) ? 1 : 0;
		}
		StratImage::deleteByStrategy($id);
		for ($i = 0; $i < count($images); $i++ ) {
					// $StrategyRulesImg->Id = $images[$i]['Id'];
			$StrategyRulesImg = new StratImage();
			$StrategyRulesImg->StrategyRulesId = $id;
			$StrategyRulesImg->Images = $images[$i]['Image'];
			$error['img'] = (!$StrategyRulesImg->create()) ? 1 : 0;
		}
	}
	return (count($error) < 0 ) ? fail("Unable to update.Please try again later!") : success("Strategy Rules has been updated");
}




	public function getStrategyRules($userId) {

		if (User::isExists($userId) && User::hasStrategyRules($userId)) {
			$strategys = StrategyRules::findByColumnArray('UserId',$userId);
			$strategyRules = array();
			$num = 0;
			foreach ($strategys as $strategy) {
				$strategyRules[] = array(
					"id" 			=> $strategy->Id,
					'num'			=> $num+1,
					"date" 			=> $strategy->updated_at,
					"likes" 		=> $strategy->getLikes($userId),
					"title" 		=> $strategy->Title,
					"images" 		=> $strategy->getImages(),
					"content"  		=> $strategy->Content,
					"user_id" 		=> $strategy->UserId,
					"comments" 		=> $strategy->getComments(),
					"isBookmark" 	=> $strategy->isBookmark()
				);
				$num++;
			}

		}
		return $strategyRules;

	}




}
