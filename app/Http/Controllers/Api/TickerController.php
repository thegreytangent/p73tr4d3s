<?php



namespace App\Http\Controllers\API;

use Auth;

use App\User;

use App\_modules;

use App\_user_maintitle;

use App\users_information;

use App\sell;

use App\watchlist;

use App\tickers;

use App\buy;

use Illuminate\Http\Request;

use App\Http\Controllers\API\BaseController as BaseController;

use Validator;

use Illuminate\Support\Facades\DB;





class TickerController extends BaseController

{

    public function GetTicker(){

        $GetTicker = tickers::select('Id as id','Code as value')->orderBy('Code', 'ASC')->get();

        return $GetTicker;

    }

}