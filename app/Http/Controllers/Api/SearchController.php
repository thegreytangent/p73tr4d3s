<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\UsersModel as _User;
use App\Http\Controllers\API\BaseController as BaseController;

class SearchController extends BaseController {


	public function SearchUser(Request $char) {
			$array = array();
			$userSearch =  _User::userSearchQuery($char->q);
			foreach ($userSearch as $user ) {
				$array[] = array(
					'id' => $user->getId(),
					'firstname' => $user->getFirstname(),
					'lastname' => $user->getLastname(),
					'img' => $user->getImage()
				);
			}

			return $array;
		}


}
