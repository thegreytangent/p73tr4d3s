<?php

namespace App\Http\Controllers\API;
use App\trading_style;
use App\Http\Controllers\API\BaseController as BaseController;

class TradingStyleController  extends BaseController
{

	public function index() {
		return trading_style::getAll();
	}
}