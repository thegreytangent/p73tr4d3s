<?php
namespace App\Http\Controllers\API;
use Auth;
use App\User;
use App\_modules;
use App\_user_maintitle;
use App\users_information;
use App\sell;
use App\watchlist;
use App\tickers;
use App\buy;
use App\trading_style;
use App\users_tradingstyle;
use App\UsersTradingStyleModel as UserTradingStyle;
use App\follows;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use Illuminate\Support\Facades\DB;


class MostActiveTradersController extends BaseController
{


    public function MostActive(){

        $users= DB::table('sell')
        ->select(DB::raw('count(*) as User, UserId'))
        ->groupBy('UserId')->orderBy('User','desc')->limit(7)
        ->get();
        foreach($users as $row) {
        $ActiveTraders= User::select(DB::raw("CONCAT(users.firstname,' ',users.Lastname)  AS fullname"),DB::raw("CONCAT('assets/img/','',users_information.Image)  AS image"),"users.id")
        ->join('users_information','users_information.UserId','=','users.id')->where('users.id',$row->UserId)->first();
        // $style = users_tradingstyle::select("*")->where('UserId',$ActiveTraders->id)->first();
        // $traders_type = trading_style::select("*")->where('id',$style->StyleId)->first();
        $follows = follows::select("*")->where('UserId',request('UserId'))->where('FollowUser',$ActiveTraders->id)->count();
        $ifollow = false;
        if($follows>=1){
            $ifollow = true;
        }
        $sells[] = array(
                'id'=>$ActiveTraders->id,
                'type'=>'Traders',
                 'fullname'=>$ActiveTraders->fullname,
                 // 'images'=>'../' . $ActiveTraders->image,
                 'images'=> users_information::getImage($ActiveTraders->id),
                 // 'traders_type'=> $traders_type->Style,
                 'traders_type'=> UserTradingStyle::tradingStyle($ActiveTraders->id),
                 'didIFollow'=>$ifollow
            );
        }



        return $sells;
    }

    public function YouMayKnow(){
        $sell = User::select('id as UserId')->orderByRaw('RAND()')->take(10)->get();
        foreach($sell as $row) {
        $ActiveTraders= User::select(DB::raw("CONCAT(users.firstname,' ',users.Lastname)  AS fullname"),DB::raw("CONCAT('assets/img/','',users_information.Image)  AS image"),"users.id as id")
        ->join('users_information','users_information.UserId','=','users.id')->where('users.id','=',$row->UserId)->first();
        // $style = users_tradingstyle::select("*")->where('UserId',$ActiveTraders->id)->first();
        // $traders_type = trading_style::select("*")->where('id',$style->StyleId)->first();
        $follows = follows::select("*")->where('UserId',request('UserId'))->where('FollowUser',$ActiveTraders->id)->count();
        $ifollow = false;
        if($follows>=1){
            $ifollow = true;
        }
        $sells[] = array(
                'id'=>$ActiveTraders->id,
                'type'=>'Traders',
                 'fullname'=>$ActiveTraders->fullname,
                 'images'=> users_information::getImage($ActiveTraders->id),
                 // 'traders_type'=> $traders_type->Style,
                  'traders_type'=> UserTradingStyle::tradingStyle($ActiveTraders->id),
                 'didIFollow'=>$ifollow
            );
        }
        return $sells;
    }




}
