<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trade_details_comments extends Model
{
    protected $table = 'trade_details_comments';

    protected $fillable = [
        // 'Image','mainTitle','AboutMe'
    ];


    public function findUserSell() {
        return self::where([
            ['UserId','=',$this->UserId],
            ['SellId','=',$this->SellId],
        ]);
    }

    public function findBySell() {
    	$result = self::where('SellId', '=', $this->SellId);
    	return ($result) ? $result : FALSE;
    }

     public  function hasComments() {
    	$result = $this->findBySell();
    	return (!$result) ? 0 : $result->count();
    }




}
