<?php
namespace App;
use App\User;
use App\Db_Objects;

class StrategyRulesLikesModel extends Db_Objects
{
  protected $table = 'strategy_rules_likes';



  public function didILike($LoginUserId) {
    return ($LoginUserId !== $this->UserId) ? FALSE : TRUE;
  }


  public function findUserStrategy() {
    return self::where([
      ['UserId','=',$this->UserId],
      ['StrategyRulesId','=',$this->StrategyRulesId],
    ]);
  }


  public function deleteLike() {
    $result =  $this->findUserStrategy()->delete();
    return (!$result) ? FALSE : TRUE;
  }


  public function insertLike() {
    $result = self::insert(array(
      'UserId' => $this->UserId,
      'StrategyRulesId' => $this->StrategyRulesId,
      'Date'   => getDateTimeNow()
    ));
    return (!$result) ? FALSE : TRUE;
  }



  public function userAlreadyLike() {
   $result =  $this->findUserStrategy()->count();
   return  ( $result <= 0 ) ? FALSE : TRUE;
 }




















}
