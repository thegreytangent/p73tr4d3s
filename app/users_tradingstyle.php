<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class users_tradingstyle extends Model
{
    protected $table = 'users_tradingstyle';

    protected $fillable = [
        'UserId','StyleId'
    ];
}
