<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Db_Objects;
use App\tickers as Tickers;


class watchlist extends Db_Objects
{
  protected $table = 'watchlist';

  protected $fillable = [
    'UserId','TickerId','Content','img_watch'
  ];



  public function ticker() {
   return Tickers::getTickerCode($this->TickerId);

 }

 public function getContentLimitBy($position) {
   return substr($this->Content, 0, $position)."...";
 }




  public function getId() {
    return $this->Id;
  }

  public function getUserId() {
    return $this->UserId;
  }

  public function getTickerId() {
    return $this->TickerId;
  }

  public function getContents() {
    return $this->Content;
  }

  public function getImage() {
    return $this->Image;
  }

  public function getimg_watch() {
    return $this->img_watch;
  }

  public function getcreated_at() {
    return $this->created_at;
  }

  public function getupdated_at() {
    return $this->updated_at;
  }

  public function setId($value) {
    return $this->Id = $value;
  }


  public function setUserId($value) {
    return $this->UserId = $value;
  }


  public function setTickerId($value) {
    return $this->TickerId = $value;
  }


  public function setContent($value) {
    return $this->Content = $value;
  }


  public function setImage($value) {
    return $this->Image = $value;
  }


  public function setimg_watch($value) {
    return $this->img_watch = $value;
  }


  public function setcreated_at($value) {
    return $this->created_at = $value;
  }


  public function setupdated_at($value) {
    return $this->updated_at = $value;
  }












  public static function dashboard_watchlist($userId) {
    $result =  self::where('UserId','=', $userId);
    return ($result) ? $result->limit(5)->get() : false;
  }

  public  function getContent() {
    $result =  self::findByColumn('UserId','=', $this->UserId);
    return ($result) ? $result : "";
  }





}
