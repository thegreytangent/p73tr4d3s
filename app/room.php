<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class room extends Model
{
    protected $table = 'room';
    protected $fillable = [
        'room_name','owner','status','key','deleted','images'
    ];
}
