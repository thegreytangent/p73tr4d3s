<?php

namespace App;
use App\users_information as UserInfo;
use App\users_links as Links;
use App\follows as Follows;
use App\UsersTradingStyleModel as TradingStyle;
use App\users_broker as UserBrokers;
use App\PrivacySettingsModel as Privacy;




use App\Db_Objects;

class UsersModel extends Db_Objects
{
  // Note: User Id is change to id

	protected $table = 'users';


	public static function find_by_username($username) {
		$result = self::findByColumn('Username',$username);
		return ($result) ? $result : false;
	}

	public static function find_by_UsernameOrId($user) {
		$column = ((int)$user) ? 'id' : 'Username';
		$result = self::findByColumn($column,$user);
		return ($result) ? $result : false;
	}



  public static function isExists($userId) {
    return self::find_by_id($userId) ? TRUE : FALSE;
  }

  public function completeName() {
    return $this->Firstname." ".$this->Lastname;
  }

  public function getId() {
    return $this->id;
  }

  public function setAboutMe($val) {
    return $this->AboutMe = $val;
  }


  public function setEmail($val) {
    return $this->email = $val;
  }


  public function setFirstname($val) {
    return $this->Firstname = $val;
  }

  public function setLastname($val) {
    return $this->Lastname = $val;
  }

  public function getImage() {
   return UserInfo::getImage($this->id);
 }

 public static function hasStrategyRules($id) {
  $result = StrategyRulesModel::findByColumnArray('UserId',$id);
  return (!$result) ? FALSE : TRUE;
}

public function getFacebook() {
 return Links::facebook($this->id);

}

public function getTwitter() {
  return Links::twitter($this->id);
}

public function getLinkedin () {
  return Links::linkedin($this->id);
}

public function getLink1 () {
  return Links::link_1($this->id);
}

public function getLink2 () {
  return Links::link_2($this->id);
}

public function getLink3 () {
  return Links::link_3($this->id);
}

public function getAboutMe() {
  return UserInfo::aboutMe($this->id);
}

public function getAddress() {
  return UserInfo::address($this->id);
}

public function getEmail() {
  return $this->email;
}


public function getFirstname() {
  return $this->Firstname;
}

public function getUsername() {
	return $this->Username;
}


public function getLastname() {
  return $this->Lastname;
}

public function countFollowers() {
  return Follows::countAllfollowers($this->id);
}


public function countFollowing() {
  return Follows::countAllfollowing($this->id);
}

public function getTradingStyle() {
  return TradingStyle::tradingStyle($this->id);
}

public function getTradingStyleId() {
 return TradingStyle::tradingStyleId($this->id);
}

public function getBrokers() {
  return UserBrokers::brokers($this->id);
}

public function update_users() {
  $result = self::update_data('id',$this->id,[
    'Firstname' => $this->Firstname,
    'Lastname' => $this->Lastname,
    'email' => $this->email,
    'updated_at' => getDateTimeNow()
  ]);
  return ($result) ? true : false;
}



public static function userSearchQuery($string) {
 $result = self::where('Firstname', 'like', '%' . $string . '%')
    ->orWhere('Lastname', 'like', '%' . $string . '%')->limit(5)->get();
  return $result;
}







// =====================  Table Data Checking ==================================

public static function hasPrivacyData($userId) {
  $result = Privacy::findByColumn('UserId',$userId);
  return ($result) ? TRUE : FALSE;
}


public  static function hasFollowed($userId) {
   $result = Follows::findByColumn('UserId',$userId);
  return ($result) ? TRUE : FALSE;
}



// =====================  end of Table Data Checking ===========================


















}
