<?php

namespace App;


use App\Db_Objects;

class users_information extends Db_Objects
{
  protected $table = 'users_information';
  public static $tempImage = 'https://cdn2.vectorstock.com/i/thumb-large/17/61/male-avatar-profile-picture-vector-10211761.jpg';

  protected $fillable = [
   'UserId','Image','Address','AboutMe'
 ];


 public function setUserId($val) {
  return $this->UserId = $val;
}

public function setImage($val) {
  return $this->Image = $val;
}

public function setAddress($val) {
  return $this->Address = $val;
}

public function setAboutMe($val) {
  return $this->AboutMe = $val;
}


public static function find_by_user($id) {
  $result = self::findByColumn('UserId',$id);
  return (!$result) ? FALSE : $result;
}

public static function address($userId) {
 $result = self::find_by_user($userId);
 return ($result) ? $result->Address : "";
}

public static function aboutMe($userId) {
 $result = self::find_by_user($userId);
 return ($result) ? $result->AboutMe : "";
}


public function image() {
 $result = self::find_by_user($this->UserId);
 return ($result) ? $result->Image : "";
}

public static function getImage($userId) {
$result = self::find_by_user($userId);
  if ($result) {
    $image = (isBase64($result->Image)) ? $result->Image : "../assets/img/".$result->Image."";
  } else {
    $image = self::$tempImage;
  }
 return $image;
}


public function update_info() {
  $result = self::update_data('UserId',$this->UserId,[
    'Image' => $this->Image,
    'Address' => $this->Address,
    'AboutMe' => $this->AboutMe,
    'updated_at' => getDateTimeNow()
  ]);
  return ($result) ? true : false;
}





}
