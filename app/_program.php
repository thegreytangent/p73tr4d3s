<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class _program extends Model
{
    protected $table = '_programs';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'programs_id', 'program_name','program_link'
    ];

}
