<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class trade_details_likes extends Model
{
    protected $table = 'trade_details_likes';

    protected $fillable = [
        'UserId','SellId'
    ];


    public function didILike($user_id) {
    	// return ($this->UserId === $user_id) ? 1 : 0;
        return 'test';
    }


    public static function countLikes($sellId) {
    	$result = self::where('SellId', '=', $sellId);
    	return (!$result) ? 0 : $result->count();
    }

    public function findUserSell() {
        return self::where([
            ['UserId','=',$this->UserId],
            ['SellId','=',$this->SellId],
        ]);
    }



    public function userAlreadyLike() {
       $result =  $this->findUserSell()->count();
       return  ( $result <= 0 ) ? FALSE : TRUE;
    }


   public function deleteLike() {
    $result =  $this->findUserSell()->delete();
    return (!$result) ? FALSE : TRUE;
    }


    public function insertLike() {
    $result = self::insert(array(
        'UserId' => $this->UserId,
        'SellId' => $this->SellId,
        'Date'   => date("Y-m-d H:i:s")
    ));
    return (!$result) ? FALSE : TRUE;
    }
}
