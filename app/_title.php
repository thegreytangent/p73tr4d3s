<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class _title extends Model
{
    protected $table = '_titles';

    protected $fillable = [
        'title_id', 'main_menu_title','program_icon'
    ];
}
