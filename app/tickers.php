<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class tickers extends Model
{
	protected $table = 'tickers';

    // protected $fillable = [
    //     'Image','mainTitle','AboutMe'
    // ];

	public static function getTickerCode($id) {
     	return DB::table('tickers')->where('id','=', $id)->first()->Code;
     }
}
