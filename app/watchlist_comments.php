<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class watchlist_comments extends Model
{
    protected $table = 'watchlist_comments';

    protected $fillable = [
        'WatchlistId','UserId','Comment'
    ];
}
