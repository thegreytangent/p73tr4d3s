<?php
namespace App;

use App\User;
use App\trading_style;
use App\Db_Objects;

class UsersTradingStyleModel extends Db_Objects
{
	protected $fillable = [
		'UserId'
	];
	protected $table = 'users_tradingstyle';

	public function setUserId($val) {
		return $this->UserId = $val;
	}

	public function setStyleId($val) {
		return $this->StyleId = $val;
	}


	public static function find_by_user($id) {
		$result = self::findByColumn('UserId',$id);
		return (!$result) ? FALSE : $result;
	}

	public static function tradingStyleId($userId) {
		$result = self::find_by_user($userId);
		return ($result) ? $result->StyleId : 1;
	}


	public static function tradingStyle($userId) {
		$result = self::find_by_user($userId);
		return ($result) ? trading_style::findByColumn('Id',self::tradingStyleId($userId))->Style : "Novice";

	}

	public function saveStyle() {
		$result = self::find_by_user($this->UserId);
		return ($result) ? $this->updateStyle() : $this->createStyle();
	}



	public function updateStyle() {
		$result = self::update_data('UserId',$this->UserId,[
			'UserId' => $this->UserId,
			'StyleId' => $this->StyleId,
			'updated_at' => getDateTimeNow()
		]);
		return ($result) ? true : false;
	}

	public function createStyle() {
		$result = self::create([
			'UserId' => $this->UserId,
			'StyleId' => $this->StyleId,
			'updated_at' => getDateTimeNow()
		]);
		return ($result) ? true : false;
	}



















}






