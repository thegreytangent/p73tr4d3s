<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\API\BaseController as BaseController;

class trade_details_images extends Model
{
	protected $table = 'trade_details_images';
	protected $fillable = [ 'Id','SellId','Image','isNewUpload'];
	protected static $bController;


	public static function isNew($id) {
		return self::find($id) ? false : true;
	}

	public static function deleteBySellId($sellId) {
		return BaseController::delete_by('SellId',$sellId);
	}



}
