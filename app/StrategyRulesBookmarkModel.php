<?php

namespace App;
use Illuminate\Support\Facades\DB;
use App\Db_Objects;

class StrategyRulesBookmarkModel extends Db_Objects
{
	protected $table = 'strategy_rules_bookmarks';

  public static function findUserBookmark($userId,$stratId) {
    $result = self::where([
       ['UserId','=',$userId],
       ['StrategyRulesId','=',$stratId],]
       )->first();
    return (!$result) ? FALSE : $result;
  }

	public function getId() {
 				return $this->Id;
	}

	public function getStrategyRulesId() {
				return $this->StrategyRulesId;
	}

	public function getUserId() {
				return $this->UserId;
	}



	public function setId($value) {
				return $this->Id = $value;
	}

	public function setStrategyRulesId($value) {
				return $this->StrategyRulesId = $value;
	}

	public function setUserId($value) {
				return $this->UserId = $value;
	}















}
