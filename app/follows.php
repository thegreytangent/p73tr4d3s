<?php
namespace App;
use App\Db_Objects;

class follows extends Db_Objects
{

  protected $table = 'follows';
  protected $fillable = [
    'UserId','FollowUser','DateTime','RefId','Status'
  ];


  public static function countAllfollowing($id) {
    $result = self::findByColumnArray('UserId',$id);
    return ($result) ? count($result) : 0;
  }


  public static function countAllfollowers($id) {
    $result = self::findByColumnArray('FollowUser',$id);
    return ($result) ? count($result) : 0;
  }


  public static function findby_UserIdFollowUser($userId, $follow) {
    $result = self::where('UserId',$userId)->where('FollowUser',$follow);
    return ($result) ? $result->first() : FALSE;
  }

  public static function follow_status($userId, $follow) {
    $follow = self::findby_UserIdFollowUser($userId, $follow);
    if ($follow) {
      $status = ($follow->Status !== 0 ) ? 'Following' : 'Pending';
    }else {
      $status = 'Follow';
    }
    return $status;


  }

  public function getStatus() {
    return $this->Status;
  }


}
