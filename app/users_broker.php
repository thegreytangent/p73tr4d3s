<?php



namespace App;



use App\Db_Objects;

use App\brokers;



class users_broker extends Db_Objects

{

	protected $table = 'users_broker';

	protected $fillable = [

		'BrokerId','UserId'

	];





	public function setUserId($val) {

		return $this->UserId = $val;

	}



	public function setBrokerId($val) {

		return $this->BrokerId = $val;

	}


	public static function hasBroker($userId) {
		$brokerArray = array();
		$findUsersBroker = self::findByColumnArray('UserId',$userId);
		foreach ($findUsersBroker as $userBroker) {
			$brokerArray[] = $userBroker->BrokerId;
		}
		 return ((array_sum($brokerArray) <= 0) || !$findUsersBroker ) ? FALSE : TRUE;
	}



	public static function brokers($userId) {
		$brokers = array();
		if (self::hasBroker($userId)) {
			foreach (self::findByColumnArray('UserId',$userId) as $broker) {
				$brokerage = brokers::find_by_id($broker->BrokerId);
				$brokers[] = array(
					'id' => $brokerage->Id,
					'name' => $brokerage->Name
				);
			}
		}
		return $brokers;
	}








	public function deleteExistingBroker() {

		return (self::delete_data('UserId',$this->UserId)) ? TRUE : FALSE;

	}





	public function createNewBroker() {

		$result = self::create([

			'UserId' => $this->UserId,

			'BrokerId' => $this->BrokerId,

			'updated_at' => getDateTimeNow()

		]);

		return ($result) ? true : false;

	}









}
