<?php

namespace App;
use App\Db_Objects;

class brokers extends Db_Objects
{
    protected $table = 'brokers';

    protected $fillable = [
        // 'UserId','TickerId','Content','img_watch'
    ];

    public static function getAll() {
    	$brokers = array();
    	foreach (self::all() as $broker ) {
    		$brokers[] = array(
    			'id' 	=> $broker->Id,
    			'name' 	=> $broker->Name
    		);
    	}
    	return $brokers;
    }





}
