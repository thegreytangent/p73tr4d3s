<?php
// Reusable functions

function getDateOnly($dateTime) {
	return date('m/d/Y', strtotime($dateTime));
}



function getDateTimeNow() {
	return date("Y-m-d H:i:s");
}

function isBase64($string) {
	$check = (strpos($string, 'base64') !== false && strpos($string,'data:image') !== false );
	return ($check) ? TRUE : FALSE;
}

function success($message="") {
	return array(
		'message' => $message,
		'result' => 'success'
	);
}


function fail($message="") {
	return array(
		'result' => 'fail',
		'message' => $message
	);
}




?>
