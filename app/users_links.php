<?php



namespace App;



use App\Db_Objects;



class users_links extends Db_Objects

{

	protected $table = 'users_links';

	// protected $fillable = [

	// 	'UserId','Facebook','Twitter','LinkedIn'

	// ];



	public function setFacebook($val) {

		return $this->Facebook = $val;

	}



	public function setTwitter($val) {

		return $this->Twitter = $val;

	}



	public function setLinkedIn($val) {

		return $this->LinkedIn = $val;

	}



	public function setLink1($val) {

		return $this->Link1 = $val;

	}



	public function setLink2($val) {

		return $this->Link2 = $val;

	}





	public function setLink3($val) {

		return $this->Link3 = $val;

	}







	public static function find_by_user($userId) {

		$result = self::findByColumn('UserId',$userId);

		return (!$result) ? FALSE : $result;

	}



	public static function facebook($userId) {

		$result = self::find_by_user($userId);

		return ($result) ? $result->Facebook : "";

	}



	public static function linkedin($userId) {

		$result = self::find_by_user($userId);

		return ($result) ? $result->LinkedIn : "";

	}



	public static function twitter($userId) {

		$result = self::find_by_user($userId);

		return ($result) ? $result->Twitter : "";

	}



	public static function link_1($userId) {

		$result = self::find_by_user($userId);

		return ($result) ? $result->Link1 : "";

	}



	public static function link_2($userId) {

		$result = self::find_by_user($userId);

		return ($result) ? $result->Link2 : "";

	}



	public static function link_3($userId) {

		$result = self::find_by_user($userId);

		return ($result) ? $result->Link3 : "";

	}







	public function update_links() {

		$result = self::update_data('UserId',$this->UserId,[

			'Facebook' => $this->Facebook,

			'Twitter' => $this->Twitter,

			'LinkedIn' => $this->LinkedIn,

			'updated_at' => getDateTimeNow()

		]);

		return ($result) ? true : false;

	}







}
