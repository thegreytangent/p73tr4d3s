<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class _modules extends Model
{
    protected $table = '_modules';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'main_menu_title', 'Id','program_name','program_icon','program_link','deleted',
    ];

    public function assignedTo()
    {
        return $this->belongsTo('App\User', 'Id');
    }

    
    public function program()
    {
        return $this->belongsTo('App\_program', 'programs_id');
    }
}

