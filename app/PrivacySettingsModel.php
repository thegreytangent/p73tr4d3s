<?php
namespace App;
use App\Db_Objects;

class PrivacySettingsModel extends Db_Objects {

  protected $table = 'privacy_settings';

  public function getEmail()
  {
    $result = self::find_by_user($this->UserId);
    return ($result) ? $this->Email : 'Hide';
  }


  public function getAddress()
  {
    $result = self::find_by_user($this->UserId);
    return ($result) ? $this->Address : 'Hide';
  }


  public function getId()
  {
    return $this->Id;
  }


  public function setId($value)
  {
    return $this->Id = $value;
  }


  public function getUserId()
  {
    return $this->UserId;
  }


  public function getTradePerformance()
  {
    $result = self::find_by_user($this->UserId);
    return ($result) ? $this->TradePerformance : 'ViewAll';
  }


  public function getTradeDetails(){
    $result = self::find_by_user($this->UserId);
    return ($result) ? $this->TradeDetails : 'Public';
  }


  public function getTradeStats() {
    $result = self::find_by_user($this->UserId);
    return ($result) ? $this->TradeStats : 'ViewAll';
  }


  public function getAccount() {
    $result = self::find_by_user($this->UserId);
    return ($result) ? $this->Account : 'Public';
  }


  public function setEmail($value){
    return $this->Email = $value;
  }


  public function setAddress($value){
    return $this->Address = $value;
  }


  public function setUserId($value){
    return $this->UserId = $value;
  }


  public function setTradePerformance($value) {
    return $this->TradePerformance = $value;
  }


  public function setTradeDetails($value){
    return $this->TradeDetails = $value;
  }


  public function setTradeStats($value) {
    return $this->TradeStats = $value;
  }


  public function setAccount($value)
  {
    return $this->Account = $value;
  }


  public function update_privacy() {
    $result = self::update_data('UserId',$this->UserId,[
      'TradePerformance'  => $this->TradePerformance,
      'TradeDetails'      => $this->TradeDetails,
      'Account'           => $this->Account,
      'Email'             => $this->Email,
      'Address'           => $this->Address,
      'TradeStats'        => $this->TradeStats,
      'updated_at'        => getDateTimeNow()
    ]);
    return ($result) ? true : false;
  }


  public function create_privacy() {
    $result = self::insert([
      'UserId'            => $this->UserId,
      'TradePerformance'  => $this->TradePerformance,
      'TradeDetails'      => $this->TradeDetails,
      'Account'           => $this->Account,
      'Email'             => $this->Email,
      'Address'           => $this->Address,
      'TradeStats'        => $this->TradeStats,
      'created_at'        => getDateTimeNow()
    ]);
    return ($result) ? true : false;
  }


}
