<?php

namespace App;
use Illuminate\Support\Facades\DB;
use App\UsersModel as User;
use App\StrategyRulesBookmarkModel as Bookmark;
use App\StrategyRulesLikesModel as Likes;
use App\StrategyRulesImagesModel as Images;
use App\StrategyRulesCommentsModel as Comments;

use App\Db_Objects;

class StrategyRulesModel extends Db_Objects {

	protected $table = 'strategy_rules';


	public function create() {
		$result = self::insert([
			'UserId' 			=> $this->UserId,
			'Title'  			=> $this->Title,
			'created_at' 	=> getDateTimeNow(),
			'updated_at' 	=> getDateTimeNow()
		]);
		return ($result) ? TRUE : FALSE;
	}

	public function isBookmark() {
		$result = Bookmark::findUserBookmark($this->UserId,$this->Id);
		return ($result) ? TRUE : FALSE;
	}

	public function updateContent() {
		$content = array('Content' => $this->Content);
		return self::update_data('Id',$this->Id,$content) ? TRUE : FALSE;
	}


	public function getComments() {

		$comments 		= array();
		$getComments 	= Comments::findByUserAndStratId($this->UserId,$this->Id);

		if ($getComments) {
			foreach ($getComments as $comment) {
				$user = User::find_by_id($comment->UserId);
				$comments[] = array(
					'id' 				=> $comment->Id,
					'user_id' 	=> $user->id,
					'user_img' 	=> $user->getImage(),
					'user' 			=> $user->completeName(),
					'comment' 	=> $comment->Comment
				);
			}
		}
		return $comments;
	}


	public function getLikes($userId) {
		$likes = new Likes();
		$likes->UserId 			= $this->UserId;
		$likes->StrategyRulesId = $this->Id;

		$findByStratId = Likes::findByColumnArray('StrategyRulesId',$this->Id);

		$count 	=  (!$findByStratId) ? 0 : count($findByStratId);
		$didIlike = $likes->userAlreadyLike();
		return [ 'countLikes' => $count,'didILike' => $didIlike];
	}


	public function getImages() {
		$images = array();
		$getImages = Images::findByColumnArray('StrategyRulesId',$this->Id);

		if ($getImages) {
			foreach ($getImages as $image ) {
				$images[] = array(
					'Id' => $image->Id,
					'Image' => $image->Images
				);
			}
		}
		return $images;
	}












}
